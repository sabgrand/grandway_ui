import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Icon from '@steroidsjs/core/ui/icon/Icon';

import './Empty.scss';


interface IEmptyProps extends IBemHocOutput {
    label?: string,
    className?: CssClassName,
}

@bem('Empty')
export default class Empty extends React.PureComponent<IEmptyProps> {
    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                <Icon 
                    className={bem.element('icon')}
                    name='empty'
                />
                <div className={bem.element('label')}>
                    {this.props.label ? this.props.label : 'Нет данных'}
                </div>
            </div>
        );
    }
}
