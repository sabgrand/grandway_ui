import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import './DataProgressBar.scss';


interface IDataProgressBarProps extends IBemHocOutput {
    fillPercentage: number,
    color?: string,
    showPercentage?: boolean,
    mode?: 'hw-payment',
    className?: CssClassName,
}

@bem('DataProgressBar')
export default class DataProgressBar extends React.PureComponent<IDataProgressBarProps> {

    static defaultProps = {
        showPercentage: false,
    }

    constructor(props) {
        super(props);
    }

    render() {
        const bem = this.props.bem;
        const fillPercentage = this.props.fillPercentage === 100 ? 101 : Math.min(100, this.props.fillPercentage);
        return (
            <div className={bem(
                bem.block({
                    [`is-${this.props.mode}`]: !!this.props.mode,
                }),
                this.props.className
            )}>
                {this.props.showPercentage && (
                    <span className={bem.element('percentage')}>
                        {fillPercentage}%
                    </span>
                )}
                <div
                    className={bem.element('body')}
                    style={{
                        backgroundColor: this.props.color,
                        width: `${Math.max(0, fillPercentage)}%`
                    }}
                />
            </div>
        );
    }
}