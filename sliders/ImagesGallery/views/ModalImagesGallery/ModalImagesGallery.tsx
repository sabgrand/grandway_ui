import * as React from 'react';
import ReactModal from 'react-modal';
import Modal from '@steroidsjs/core/ui/modal/Modal';
import {IModalViewProps} from '@steroidsjs/core/ui/modal/Modal/Modal';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import IGalleryItem from '../../../../types/IGalleryItem';
import ImagesGallery from '../../ImagesGallery';

import './ModalImagesGallery.scss';

interface IModalImagesGalleryProps extends IModalViewProps, IBemHocOutput {
    galleryItems: IGalleryItem[],
    currentSlide: number,
    onClose: () => void,
    children: React.ReactNode,
    isPhone: boolean,
}

@bem('ModalImagesGallery')
class ModalImagesGalleryView extends React.PureComponent<IModalImagesGalleryProps> {
    render() {
        const bem = this.props.bem;
        const overrideDefaultClasses = {
            base: bem.block('overlay'),
            afterOpen: `${bem.block()}_overlay-after`,
            beforeClose: `${bem.block()}_overlay-before`,
        };

        return (
            <ReactModal
                {...this.props}
                ariaHideApp={false}
                isOpen={!this.props.isClosing}
                closeTimeoutMS={300}
                overlayClassName={overrideDefaultClasses}
                className={bem(
                    bem.element('body', {size: this.props.size}),
                    this.props.className,
                )}
                bodyOpenClassName={bem.block('body-opened')}
                onRequestClose={this.props.onClose}
                shouldCloseOnEsc
                shouldCloseOnOverlayClick
            >
                <div className={bem.element('content')}>
                    {this.props.children}
                    <button
                        className={bem.element('close')}
                        onClick={e => {
                            e.preventDefault();
                            this.props.onClose();
                        }}
                        aria-label={__('Закрыть')}
                    />
                </div>
            </ReactModal>
        );
    }
}

export default function ModalImagesGallery(props: IModalImagesGalleryProps) {
    return (
        <Modal
            {...props}
            onClose={props.onClose}
            view={ModalImagesGalleryView}
        >
            <ImagesGallery
                initialSlide={props.currentSlide}
                galleryItems={props.galleryItems}
                isPhone={props.isPhone}
                mode='fullscreen'
            />
        </Modal>
    );
}
