import * as React from 'react';
import IGalleryItem from '../../../../types/IGalleryItem';

const Image = (props: {
    bem: any,
    index: number,
    showBackground: boolean,
    image: IGalleryItem,
}) => {
    const bem = props.bem;
    return (
        <div className={bem.element('image-container')}>
            {props.showBackground && (
                <span
                    className={bem.element('image-background')}
                    style={{backgroundImage: `url(${props.image.full.url})`}}
                />
            )}
            <img
                className={bem.element('image')}
                src={props.image.full.url}
                alt={__('Изображение объекта {number}', {number: props.index + 1})}
            />
        </div>
    );
};

const YOUTUBE_EMBED_LINK = 'https://www.youtube.com/embed/';

const Video = (props: {
    bem: any,
    video: IGalleryItem,
    playingVideo: boolean,
    isPhone: boolean,
}) => {
    const bem = props.bem;
    const videoUrl = `${YOUTUBE_EMBED_LINK}${props.video.videoId}`;

    if (props.isPhone) {
        return (
            <a
                href={videoUrl}
                target='_blank'
                rel='noreferrer'
            >
                <div className={bem.element('video-play-thumbnail', 'full-height')} />
            </a>
        );
    }

    if (props.playingVideo) {
        return (
            <div className={bem.element('video-container')}>
                <iframe
                    width="100%"
                    height="100%"
                    src={videoUrl}
                    title="YouTube video player"
                    frameBorder="0"
                    allow="accelerometer; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                />
            </div>
        );
    }

    return <div className={bem.element('video-play-thumbnail', 'full-height')} />;
};

interface ISlideItemProps {
    bem: any,
    galleryItem: IGalleryItem,
    currentIndex: number,

    playingVideo: boolean,
    setPlayingVideo: () => void,
    openGalleryModal: () => void,

    isDefaultMode: boolean,
    isPhone: boolean,
}

export default function SlideItem(props: ISlideItemProps) {
    const bem = props.bem;
    const {
        galleryItem,
        currentIndex,

        playingVideo,
        setPlayingVideo,
        openGalleryModal,

        isPhone,
        isDefaultMode,
    } = props;

    if (galleryItem.type === 'video') {
        return (
            <div
                className={bem.element('slide-wrapper')}
                onClick={() => setPlayingVideo()}
            >
                <Video
                    bem={bem}
                    video={galleryItem}
                    playingVideo={playingVideo}
                    isPhone={isPhone}
                />
            </div>
        );
    }

    return (
        <div
            className={bem.element('slide-wrapper')}
            onClick={isDefaultMode ? () => openGalleryModal() : null}
        >
            <Image
                bem={bem}
                index={currentIndex}
                showBackground={isDefaultMode}
                image={galleryItem}
            />
        </div>
    );
}
