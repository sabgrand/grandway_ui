import * as React from 'react';
import {RefObject} from 'react';
import Slider from 'react-slick';
import {openModal} from '@steroidsjs/core/actions/modal';
import Icon from '@steroidsjs/core/ui/icon/Icon';
import {bem, connect} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IConnectHocOutput} from '@steroidsjs/core/hoc/connect';
import IGalleryItem from '../../types/IGalleryItem';
import SlideItem from './views/SlideItem';
import ModalImagesGallery from './views/ModalImagesGallery';

import './ImagesGallery.scss';

interface IImagesGalleryProps extends IBemHocOutput, IConnectHocOutput {
    galleryItems: IGalleryItem[],
    initialSlide?: number,
    mode?: 'default' | 'fullscreen',
    isPhone: boolean,
    verticalDividingLine?: boolean,
}

interface ISlickArrowWrapperProps {
    currentSlide?: number,
    slideCount?: number,
    children: React.ReactNode,
    [key: string]: any,
}

interface IImagesGalleryState {
    playingVideo: boolean,
    currentSlide: number,
}

/*
* Библиотека react-slick передает внутрь кастомных стрелок свойства currentSlide и slideCount, которые
* преобразуются в кастомные атрибуты html-тегов, что выдает ошибку.
* Данная обертка удаляет эти свойства из объекта props, что позволяет избежать ошибки.
* */
const SlickArrowWrapper = ({ currentSlide, slideCount, children, ...props }: ISlickArrowWrapperProps) => (
    <span {...props}>{children}</span>
);

@connect()
@bem('ImagesGallery')
export default class ImagesGallery extends React.PureComponent<IImagesGalleryProps, IImagesGalleryState> {
    static defaultProps = {
        mode: 'default',
    }

    _sliderRef: RefObject<any>;

    constructor(props) {
        super(props);

        this.state = {
            playingVideo: false,
            currentSlide: 0,
        };

        this._sliderRef = React.createRef();
    }

    componentDidMount() {
        if (this.props.initialSlide && this._sliderRef.current) {
            this._sliderRef.current.slickGoTo(this.props.initialSlide, true);
        }
    }

    render() {
        const bem = this.props.bem;

        const galleryItems = this.props.galleryItems;

        const sliderSettings = {
            className: bem.element('slider', {
                default: this.props.mode === 'default',
                fullscreen: this.props.mode === 'fullscreen',
                empty: galleryItems.length === 0,
            }),
            prevArrow: (
                <SlickArrowWrapper>
                    <Icon
                        className={bem.element('arrow-icon', 'prev')}
                        name='arrow_slider'
                    />
                </SlickArrowWrapper>
            ),
            nextArrow: (
                <SlickArrowWrapper>
                    <Icon
                        className={bem.element('arrow-icon', 'next')}
                        name='arrow_slider'
                    />
                </SlickArrowWrapper>
            ),
            customPaging: (index) => {
                if (galleryItems[index].type === 'video') {
                    return <div className={bem.element('video-play-thumbnail')} />;
                }
                return (
                    <img
                        className={bem.element('paging-preview')}
                        src={galleryItems[index].thumbnail.url}
                        alt={__('Изображение объекта {number}', {number: index + 1})}
                    />
                );
            },
            beforeChange: (oldIndex, newIndex) => {
                this.setState({currentSlide: newIndex});
                if (this.state.playingVideo) {
                    this.setState({playingVideo: false});
                }
            },
            arrows: !this.state.playingVideo,
            draggable: false,
            dots: true,
            fade: false,
            infinite: true,
            speed: 0,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        dots: false,
                        draggable: true,
                        infinite: false,
                    },
                },
            ],
            ref: this._sliderRef,
        };

        return (
            <div className={bem.block()}>
                <Slider {...sliderSettings}>
                    {galleryItems.map((galleryItem, index) => (
                        <SlideItem
                            key={index}
                            bem={bem}
                            galleryItem={galleryItem}
                            currentIndex={index}
                            isDefaultMode={this.props.mode === 'default'}
                            isPhone={this.props.isPhone}
                            playingVideo={this.state.playingVideo}
                            setPlayingVideo={() => this.setState({playingVideo: true})}
                            openGalleryModal={() => this.props.dispatch(openModal(ModalImagesGallery, {
                                currentSlide: this.state.currentSlide,
                                isPhone: this.props.isPhone,
                                galleryItems,
                            }))}
                        />
                    ))}
                </Slider>
                {this.props.isPhone && this.props.mode === 'default' && (
                    <span className={bem.element('slides-counter')}>
                        {`${this.state.currentSlide + 1}${this.props.verticalDividingLine ? '/' : ' - '}${galleryItems.length}`}
                    </span>
                )}
            </div>
        );
    }
}
