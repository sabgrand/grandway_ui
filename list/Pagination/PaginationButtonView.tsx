import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IPaginationViewProps} from '@steroidsjs/core/ui/list/Pagination/Pagination';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Icon from '@steroidsjs/core/ui/icon/Icon/index';

@bem('PaginationButtonView')
export default class PaginationButtonView extends React.PureComponent<IPaginationViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <ul className={bem(
                this.props.className,
                bem.block(),
                'flex-row',
                'pagination',
                `pagination-${this.props.size}`
            )}>
                {this.props.pages.map((item, index) => (
                    <li
                        key={index}
                        className={bem(
                            bem.element('page', {hidden: !item.page}),
                            'page-item',
                            item.isActive ? 'active' : ''
                        )}
                        onClick={e => {
                            e.preventDefault();
                            this.props.onSelect(item.page);
                        }}
                    >
                        <span
                            className={bem(
                                bem.element('page-link', {hidden: !item.page}),
                                'page-link'
                            )}
                        >
                            {item.label}
                        </span>
                    </li>
                ))}
                <li
                    className={bem(bem.element('page', {disabled: this.props.page === this.props.totalPages}))}
                >
                    <span
                        className={bem(
                            bem.element('page-link'),
                            'page-link'
                        )}
                        onClick={e => {
                            e.preventDefault();
                            this.props.onSelect(this.props.page + 1);
                        }}
                    >
                        <Icon
                            className={bem.element('link-icon')}
                            name='chevron_right'
                        />
                    </span>
                </li>
            </ul>
        );
    }

}
