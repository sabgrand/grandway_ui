import * as React from 'react';
import throttle from 'lodash/throttle';

import {bem} from '@steroidsjs/core/hoc';
import {IPaginationViewProps} from '@steroidsjs/core/ui/list/Pagination/Pagination';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

@bem('PaginationEndlessView')
export default class PaginationEndlessView extends React.PureComponent<IPaginationViewProps & IBemHocOutput> {
    _ref: React.RefObject<HTMLDivElement>;

    constructor(props) {
        super(props);
        this._ref = React.createRef();
    }

    getElementTopPosition(elm) {
        let yPos = 0;

        if (elm && elm.offsetTop !== undefined && elm.clientTop !== undefined) {
            yPos += (elm.offsetTop + elm.clientTop) + this.getElementTopPosition(elm.offsetParent);
        }

        return yPos;
    }

    handleScroll = throttle(() => {
        const pageYOffset = window.pageYOffset + window.innerHeight;
        const paginationPosition = this.getElementTopPosition(this._ref.current);

        if (pageYOffset > paginationPosition) {
            this.props.onSelectNext();
        }
    }, 100);

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    render() {
        const bem = this.props.bem;

        return (
            <div className={bem.block()} ref={this._ref}/>
        );
    }

}
