import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import {IGridViewProps} from '@steroidsjs/core/ui/list/Grid/Grid';
import LoaderView from '../../layout/Loader/LoaderView';

import _isString from 'lodash-es/isString';

import './GridMobileView.scss';

@bem('GridMobileView')
export default class GridMobileView extends React.Component<IGridViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;

        return (
            <div className={bem(bem.block({loading: this.props.isLoading}), this.props.className)}>
                {this.props.outsideSearchFormNode}
                {this.props.paginationSizeNode}
                <div>
                    {this.renderTable()}
                    {
                        this.props.isLoading
                            ? <LoaderView/>
                            : this.props.paginationNode
                    }
                </div>
            </div>
        );
    }

    renderTable() {
        const bem = this.props.bem;
        return (
            <table className={bem.element('table')}>
                <thead className={bem.element('table-header')}>
                    <tr>
                        {this.props.columns.map((column, columnIndex) => (
                            <th
                                key={columnIndex}
                                className={column.headerClassName}
                            >
                                {column.label}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody className={bem.element('table-body')}>
                    {(this.props.items && !this.props.emptyNode) && this.props.items.map((item, rowIndex) => (
                        <tr key={item[this.props.primaryKey] || rowIndex}>
                            {this.props.columns.map((column, columnIndex) => (
                                <td
                                    key={columnIndex}
                                    className={column.className}
                                    data-label={_isString(column.label) ? column.label : null}
                                >
                                    {this.props.renderValue(item, column)}
                                </td>
                            ))}
                        </tr>
                    ))}
                    {(!this.props.items && this.props.emptyNode) && (
                        <tr>
                            <td colSpan={this.props.columns.length}>
                                {this.props.emptyNode}
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        );
    }

}
