import Enum from '@steroidsjs/core/base/Enum';

export default class PeriodEnum extends Enum {

    static MONTH = 'month';
    static YEAR = 'year';
    static ALL = 'all';
    static INTERVAL = 'interval';

    static getLabels() {
        return {
            [this.MONTH]: __('За месяц'),
            [this.YEAR]: __('За год'),
            [this.ALL]: __('За все время'),
            [this.INTERVAL]: __('Интервал'),
        };
    }

    static getShortLabels() {
        return {
            //TODO Возможно нужно будет это удалить, если за месяц ничего выводиться не будет
            // [this.MONTH]: __('За текущий месяц'),
            [this.YEAR]: __('За год'),
            [this.ALL]: __('За все время'),
            [this.INTERVAL]: __('Интервал'),
        };
    }

    static getItems() {
        return Object.keys(this.getLabels()).map(id => ({
            id,
            label: this.getLabel(id),
        }));
    }

    static getShortItems() {
        return Object.keys(this.getShortLabels()).map(id => ({
            id,
            label: this.getLabel(id),
        }));
    }

}