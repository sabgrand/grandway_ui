export interface IGraph {
    area: {
        id: number,
        label: string,
        color: string,
        parentId: number
    },
    percent: number | string,
    subAllocations?: IGraph[]
}