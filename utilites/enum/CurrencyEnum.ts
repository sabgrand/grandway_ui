import _round from 'lodash-es/round';
import Enum from '@steroidsjs/core/base/Enum';

export default class CurrencyEnum extends Enum {

    static USD = 'usd';
    static RUB = 'rub';
    static EUR = 'eur';
    static PERCENT = '%';
    static UNIT_PERSONAL = 'unit_personal';
    static UNIT_STRUCTURE = 'unit_structure';
    static UNIT_TOTAL = 'unit_total';

    static getLabels() {
        return {
            [this.USD]: __('Доллары'),
            [this.RUB]: __('Рубли'),
            [this.EUR]: __('Евро'),
            [this.UNIT_PERSONAL]: __('Личные единицы'),
            [this.UNIT_STRUCTURE]: __('Структурные единицы'),
            [this.UNIT_TOTAL]: __('Нетто единицы'),
        };
    }

    static getSymbols() {
        return {
            [this.USD]: __('$'),
            [this.RUB]: __('₽'),
            [this.EUR]: __('€'),
            [this.PERCENT]: __('%'),
            [this.UNIT_PERSONAL]: __('ЛЕ'),
            [this.UNIT_STRUCTURE]: __('СЕ'),
            [this.UNIT_TOTAL]: __('НЕ'),
        };
    }

    static getSymbol(id) {
        return this.getSymbols()[id] || '';
    }

    static formatUsd(amount, asFloat = true) {
        return this.format(amount, this.USD, asFloat);
    }

    static formatRub(amount, asFloat = true) {
        return this.format(amount, this.RUB, asFloat);
    }

    static formatUnitPersonal(amount, asFloat = true) {
        return this.format(amount, this.UNIT_PERSONAL, asFloat);
    }

    static formatUnitStructure(amount, asFloat = true) {
        return this.format(amount, this.UNIT_STRUCTURE, asFloat);
    }

    static formatUnitTotal(amount, asFloat = true) {
        return this.format(amount, this.UNIT_TOTAL, asFloat);
    }

    static format(amount, currency, asFloat = false, scale = 0) {

        if (scale > 0) {
            const scaleFormat = (scale === 0)
                ? ''
                : '::.' + '0'.repeat(scale);

            return __('{amount, number, '+scaleFormat+'} {symbol}', {
                amount: _round(amount, 2),
                symbol: this.getSymbol(currency) || currency.toUpperCase()
            });
        }

        return __('{amount, number} {symbol}', {
            amount: _round(amount, asFloat ? 2 : 0),
            symbol: this.getSymbol(currency) || currency.toUpperCase()
        });
    }

    static formatReverseSymbol(amount, currency, asFloat = false) {
        return __('{symbol}{amount, number}', {
            amount: amount ? _round(amount, asFloat ? 2 : 0) : 0,
            symbol: this.getSymbol(currency) || currency.toUpperCase()
        });
    }

}