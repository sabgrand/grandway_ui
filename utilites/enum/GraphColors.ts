import Enum from '@steroidsjs/core/base/Enum';

export default class GraphColors extends Enum {

    static GREEN = 'green';
    static TURQUOISE = 'turquoise';
    static YELLOW = 'yellow';
    static PURPLE = 'purple';
    static BLUE = 'blue';
    static BLUE_3 = 'blue-3';
    static BLUE_DARK_2 = 'blue-dark-2';

    static EMPTY_BLUE = 'empty-blue'
    static EMPTY_BLUE_LIGHT = 'empty-blue-light'

    static getLabels() {
        return {
            [this.GREEN]: '#11d982',
            [this.TURQUOISE]: '#28bd96',
            [this.YELLOW]: '#ffd912',
            [this.PURPLE]: '#742aee',
            [this.BLUE]: '#4c5eff',
            [this.BLUE_3]: '#c6c8ff',
            [this.BLUE_DARK_2]: '#323448',

            [this.EMPTY_BLUE]: '#e9eafd',
            [this.EMPTY_BLUE_LIGHT]: '#f1f2fc',
        };
    }

    static getColor() {
        return {
            ['#11d982']: this.GREEN,
            ['#28bd96']: this.TURQUOISE,
            ['#ffd912']: this.YELLOW,
            ['#742aee']: this.PURPLE,
            ['#4c5eff']: this.BLUE,
            ['#c6c8ff']: this.BLUE_3,
            ['#323448']: this.BLUE_DARK_2,

            ['#e9eafd']: this.EMPTY_BLUE,
            ['#f1f2fc']: this.EMPTY_BLUE_LIGHT,
        };
    }

    static getColorByHex(hex) {
        return this.getColor()[hex] || '';
    }
}