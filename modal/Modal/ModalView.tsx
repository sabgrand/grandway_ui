import * as React from 'react';
import Modal from 'react-modal';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IModalViewProps} from '@steroidsjs/core/ui/modal/Modal/Modal';
import Controls from '@steroidsjs/core/ui/nav/Controls';

interface IModalViewPrivateProps extends IModalViewProps, IBemHocOutput {
    showClose?: boolean,
}

@bem('ModalView')
export default class ModalView extends React.PureComponent<IModalViewPrivateProps> {

    static defaultProps = {
        showClose: true,
    }

    render() {
        const bem = this.props.bem;
        const overrideDefaultClasses = {
            base: bem.block('overlay'),
            afterOpen: bem.block('overlay-after'),
            beforeClose: bem.block('overlay-before')
        };
        return (
            <div>
                <Modal
                    {...this.props}
                    isOpen={!this.props.isClosing}
                    closeTimeoutMS={200}
                    overlayClassName={overrideDefaultClasses}
                    className={bem(
                        bem.element('body', {
                            'with-title': !!this.props.title,
                            [this.props.size]: !!this.props.size,
                        }),
                        this.props.className
                    )}
                    bodyOpenClassName={bem.block('body-opened')}
                    ariaHideApp={false}
                >
                    {this.props.showClose && (
                        <a
                            className={bem.element('close')}
                            href='#'
                            onClick={e => {
                                e.preventDefault();
                                this.props.onClose();
                            }}
                        />
                    )}
                    {this.props.title && (
                        <div className={bem.element('header')}>
                            <h1 className={bem.element('title')}>
                                {this.props.title}
                            </h1>
                        </div>
                    )}
                    <div className={bem.element('content')}>
                        {this.props.children}
                    </div>
                    {this.props.controls && (
                        <div className={bem.element('footer')}>
                            <Controls
                                items={this.props.controls}
                                className={bem.element('controls')}
                            />
                        </div>
                    )}
                </Modal>
            </div>
        );
    }

}
