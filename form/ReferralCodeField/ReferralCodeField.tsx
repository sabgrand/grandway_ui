import * as React from 'react';
import {formValueSelector} from 'redux-form';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {bem, connect, form, components} from '@steroidsjs/core/hoc';
import {InputField} from '@steroidsjs/core/ui/form';
import {IInputFieldProps} from '@steroidsjs/core/ui/form/InputField/InputField';

import {REFERRAL_CODE_LENGTH} from '../../config/constants';
import Avatar from '../../user/Avatar';

import './ReferralCodeField.scss';

interface IReferralCodeFieldProps extends IBemHocOutput {
    inputProps?: IInputFieldProps,
    className?: string,

    referralCode?: string,
}

interface IUserReferral {
    referralCode: string,
    name: string,
    avatarUrl: string,
}

interface IReferralCodeFieldState {
    receiver?: IUserReferral,
    isLoading: boolean,
}

const getAttribute = props => props.inputProps?.attribute || 'referralCode';

@form()
@components('http')
@connect(
    (state, props) => ({
        referralCode: formValueSelector(props.formId)(state, getAttribute(props)),
    })
)
@bem('ReferralCodeField')
export default class ReferralCodeField extends React.PureComponent<IReferralCodeFieldProps, IReferralCodeFieldState> {

    constructor(props) {
        super(props);

        this.state = {
            receiver: null,
            isLoading: false,
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.referralCode !== this.props.referralCode) {
            this._fetchReceiver(this.props.referralCode);
        }
    }

    render() {
        const bem = this.props.bem;
        return (
            <div
                className={bem(bem.block(), this.props.className)}
            >
                <InputField
                    type='text'
                    placeholder='000 000 000'
                    maskProps={{
                        mask: '999 999 999'
                    }}
                    layout='mb-5'
                    required
                    {...this.props.inputProps}
                    attribute={getAttribute(this.props)}
                />
                <div className={bem.element('receiver-preview')}>
                    {this.state.isLoading
                        ? '...'
                        : (this.state?.receiver && (
                            <>
                                <Avatar
                                    className='mr-5'
                                    avatarUrl={this.state.receiver.avatarUrl}
                                />
                                {this.state.receiver.name}
                            </>
                        ))
                    }
                </div>
            </div>
        );
    }

    async _fetchReceiver(code) {
        code = code?.replace(/[^0-9]+/g, '');
        if (this.state.receiver && this.state.receiver.referralCode === code) {
            return;
        }

        if (!code || code.length !== REFERRAL_CODE_LENGTH) {
            this.setState({
                receiver: null,
            });
            return;
        }

        this.setState({
            receiver: null,
            isLoading: true,
        });
        try {
            const receiver = await this.props.http.get(`/api/v1/users/by-referral/${code}`, {}, {lazy: true});
            this.setState({
                isLoading: false,
                receiver: receiver || null,
            });
        } catch (e) {
            this.setState({
                isLoading: false,
            });
        }
    }
}
