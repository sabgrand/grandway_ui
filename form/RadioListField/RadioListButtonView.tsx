import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IRadioListFieldViewProps} from '@steroidsjs/core/ui/form/RadioListField/RadioListField';
import {ReactText} from 'react';

@bem('RadioListFieldView')
export default class RadioListButtonView extends React.PureComponent<IRadioListFieldViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                {this.props.items.map(item => (
                    <div
                        key={item.id as ReactText}
                        className={bem.element('list-button', {
                            'checked': item.isSelected,
                            'disabled': this.props.disabled,
                        })}
                        onClick={() => this.props.onItemClick(item)}
                    >
                        <input
                            {...this.props.inputProps}
                            id={this.props.fieldId + '_' + item.id}
                            className={bem(
                                bem.element('input', {
                                    'is-invalid': this.props.isInvalid,
                                }),
                            )}
                            checked={item.isSelected}
                            disabled={this.props.disabled}
                        />
                        <label
                            htmlFor={this.props.fieldId + '_' + item.id}
                        >
                            {item.label}
                        </label>
                    </div>
                ))}
            </div>
        );
    }

}
