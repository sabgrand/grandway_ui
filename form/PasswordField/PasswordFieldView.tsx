import * as React from 'react';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IPasswordFieldViewProps} from '@steroidsjs/core/ui/form/PasswordField/PasswordField';
import Icon from '@steroidsjs/core/ui/icon/Icon';

@bem('PasswordFieldView')
export default class PasswordFieldView extends React.PureComponent<IPasswordFieldViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div
                className={bem(
                    bem.block(),
                    this.props.className
                )}
            >
                <div
                    className={bem(
                        bem.element('container', {
                            'disabled': this.props.inputProps.disabled,
                            'is-invalid': this.props.isInvalid
                        }),
                    )}
                >
                    <input
                        className={bem.element('input')}
                        {...this.props.inputProps}
                    />
                    {this.props.security && (
                        <span
                            className={bem.element('icon-eye')}
                            onMouseDown={this.props.onShowPassword}
                            onMouseUp={this.props.onHidePassword}
                            onTouchStart={this.props.onShowPassword}
                            onTouchEnd={this.props.onHidePassword}
                        >
                            <Icon
                                name={this.props.inputProps.type === 'password' ? 'security_eye' : 'security_eye_slash'}
                            />
                        </span>
                    )}
                </div>
            </div>
        );
    }

}
