import * as React from 'react';
import PasswordField from '@steroidsjs/core/ui/form/PasswordField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const passwordStates = [
    'Initial',
    'Disabled',
];

export const Password = () => (
    <StoryPage
        headerTitle='Поле PasswordField'
        headerDescription={[
            'Поле для ввода пароля.',
            __('Свойтсво {prop} обязательно для появления иконки "отображения" пароля.', {
                prop: <span className='story-text-prop'>security</span>
            })
        ]}
    >
        {passwordStates.map((type, index) => (
            <StorySection
                headerTitle={`Состояние ${type}`}
                key={index}
            >
                <div
                    className='col-2'
                >
                    <PasswordField
                        label={__('Label')}
                        placeholder={__('Введите пароль')}
                        disabled={type === 'Disabled'}
                        security
                    />
                </div>
            </StorySection>
        ))}
    </StoryPage>
);