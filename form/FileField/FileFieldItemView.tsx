import * as React from 'react';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IFileFieldItemViewProps} from '@steroidsjs/core/ui/form/FileField/FileField';

@bem('FileFieldItemView')
export default class FileFieldItemView extends React.PureComponent<IFileFieldItemViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem.block({error: !!this.props.error})}>
                <div className={bem.element('body')}>
                    <div className={bem.element('preview')}>
                        {this.props.image && (
                            <img
                                className={bem.element('image')}
                                src={this.props.image.url}
                                alt={this.props.title}
                            />
                        )}
                    </div>
                    <p className={bem.element('title')}>
                        {this.props.error || this.props.title}
                    </p>
                </div>
                {this.props.showRemove && (
                    <div
                        className={bem.element('remove')}
                        onClick={this.props.onRemove}
                    >
                        {__('Удалить')}
                    </div>
                )}
            </div>
        );
    }

}
