import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Button from '@steroidsjs/core/ui/form/Button';
import Icon from '@steroidsjs/core/ui/icon/Icon';
import {IFileFieldViewProps} from '@steroidsjs/core/ui/form/FileField/FileField';

interface IFileFieldItemViewProps extends IFileFieldViewProps, IBemHocOutput {
    showItemView?: boolean,
}

@bem('FileFieldView')
export default class FileFieldView extends React.PureComponent<IFileFieldItemViewProps> {

    static defaultProps = {
        showItemView: true,
    }

    render() {
        const bem = this.props.bem;
        const ButtonComponent = this.props.buttonComponent;
        const FileItemView = this.props.itemView;
        return (
            <div className={bem.block()}>
                {this.props.showItemView && this.props.items !== null && this.props.items.length > 0 && this.props.items.map(item => (
                    <FileItemView
                        key={item.uid}
                        {...item}
                        {...this.props.itemProps}
                    />
                )) || (
                    this.props.buttonComponent && <ButtonComponent {...this.props.buttonProps} />
                    || (
                        <div className={bem.element('button')}>
                            <Button
                                {...this.props.buttonProps}
                                label={null}
                                size='lg'
                                block
                            >
                                <Icon
                                    className={bem.element('button-icon')}
                                    name='file_photo'
                                />
                                <span className={bem.element('button-label-container')}>
                                    <span className={bem.element('button-label')}>
                                        {this.props.buttonProps.label}
                                    </span>
                                    {/*@ts-ignore*/}
                                    {this.props.buttonProps.subLabel && (
                                        <span className={bem.element('button-sublabel')}>
                                            {/*@ts-ignore*/}
                                            {this.props.buttonProps?.subLabel}
                                        </span>
                                    )}
                                </span>
                            </Button>
                        </div>
                    )
                )}
            </div>
        );
    }

}
