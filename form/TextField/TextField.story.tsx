import * as React from 'react';
import TextField from '@steroidsjs/core/ui/form/TextField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

export default {
    title: 'Form/TextField',
};

const inputTypes = [
    'default',
];

const inputStates = [
    'Initial',
    'Error',
];

export const Default = () => (
    <StoryPage
        headerTitle='Поле TextField'
        headerDescription={[
            'Текстовое поле для формы.'
        ]}
    >
        {inputTypes.map((type, index) => (
            <StorySection
                headerTitle={`Тип ${type}`}
                key={index}
            >
                {inputStates.map((state, index) => (
                    <div
                        className='col-3'
                        key={index}
                    >
                        <h6>
                            Состояние "{state}"
                        </h6>
                        <TextField
                            attribute='text'
                            label={__('Label')}
                            placeholder='Text'
                            inputProps={state === 'Error' && {
                                value: 'Hello. My name is John Randle. I work at the American Embassy in Prague. DSfsdf'
                            }}
                            errors={state === 'Error' && [
                                '263 из 250 символов'
                            ]}
                        />
                    </div>
                ))}
            </StorySection>
        ))}
    </StoryPage>
);