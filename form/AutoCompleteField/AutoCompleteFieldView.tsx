import * as React from 'react';
import {ReactText} from 'react';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IAutoCompleteFieldViewProps} from '@steroidsjs/core/ui/form/AutoCompleteField/AutoCompleteField';

@bem('AutoCompleteFieldView')
export default class AutoCompleteFieldView extends React.PureComponent<IAutoCompleteFieldViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div
                className={bem.block({
                    disabled: this.props.disabled
                })}
            >
                <input
                    {...this.props.inputProps}
                    className={bem(
                        bem.element('input'),
                        this.props.isInvalid && 'is-invalid',
                        this.props.inputProps.className,
                        this.props.className
                    )}
                    onChange={e => this.props.input.onChange(e.target.value)}
                    placeholder={this.props.placeholder}
                    disabled={this.props.disabled}
                    required={this.props.required}
                />
                {this.props.isOpened && (
                    <div className={bem.element('drop-down')}>
                        <div className={bem.element('drop-down-list')}>
                            {this.props.items.map(item => (
                                <div
                                    key={item.id as ReactText}
                                    className={bem.element('drop-down-item', {hover: item.isHovered, select: item.isSelected})}
                                    onClick={() => this.props.onItemClick(item)}
                                    onMouseOver={() => this.props.onItemMouseOver(item)}
                                >
                                    {item.label}
                                </div>
                            ))}
                        </div>
                    </div>
                )}
            </div>
        );
    }

}
