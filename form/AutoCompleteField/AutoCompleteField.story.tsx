import * as React from 'react';
import AutoCompleteField from '@steroidsjs/core/ui/form/AutoCompleteField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

export default {
    title: 'Form/AutoCompleteField',
};

const cityArray = [
    'Москва',
    'Сантк-Петербург',
    'Красноярск',
    'Краснодар',
    'Новосибирск',
    'Екатеринбург',
];

const _onSearch = (action, params) => {
    return cityArray.filter(city => city.includes(params.query));
};

export const Autocomplete= () => (
    <StoryPage
        headerTitle='Поле Autocomplete.'
        headerDescription={[
            'При заполнении отображает список всех величин, подходящих под условия поиска, в виде DropDownField.'
        ]}
    >
        <StorySection
            headerTitle='Заполните поле для поиска города.'
        >
            <div className='col-2'>
                <AutoCompleteField
                    attribute='search'
                    label={__('Ваш город')}
                    placeholder={__('Начните искать...')}
                    dataProvider={{
                        onSearch: _onSearch,
                    }}
                />
            </div>
        </StorySection>
    </StoryPage>
);