import * as React from 'react';
import _isString from 'lodash-es/isString';

import {bem} from '@steroidsjs/core/hoc';
import {IButtonViewProps} from '@steroidsjs/core/ui/form/Button/Button';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import Icon from '@steroidsjs/core/ui/icon/Icon';

interface IButtonViewPrivateProps extends IButtonViewProps, IBemHocOutput{
    iconPosition?: 'left' | 'right',
    linkAsButton?: boolean,

    // Если нужно свзять кнопку с формой, при этом кнопка находится вне формы
    customFormId?: string,
}

@bem('ButtonView')
export default class ButtonView extends React.PureComponent<IButtonViewPrivateProps> {

    static defaultProps = {
        iconPosition: 'right',
        linkAsButton: false,
    };

    render() {
        return this.props.tag === 'a' ? this.renderLink() : this.renderButton();
    }

    renderLink() {
        return (
            <a
                className={this._getClassName({
                    link: true,
                })}
                href={this.props.url}
                onClick={
                    this.props.disabled
                        ? (e) => e.preventDefault()
                        : this.props.onClick
                }
                style={this.props.style}
                target={this.props.target}
            >
                {this.renderLabel()}
                {this.renderBadge()}
            </a>
        );
    }

    renderButton() {
        return (
            <button
                type={this.props.type}
                disabled={this.props.disabled}
                onClick={this.props.onClick}
                form={this.props.customFormId}
                style={this.props.style}
                className={this._getClassName()}
            >
                {this.renderLabel()}
                {this.renderBadge()}
            </button>
        );
    }

    renderLabel() {
        const bem = this.props.bem;
        const title = this.props.label && _isString(this.props.label)
            ? this.props.label
            : (this.props.hint || null);
        return (
            <>
                {this.props.isLoading && (
                    <div className={bem.element('preloader')}>
                        <span
                            className='spinner-border spinner-border-sm'
                            role='status'
                            aria-hidden='true'
                        />
                    </div>
                )}
                {(this.props.showLabelOnLoading || !this.props.isLoading) && (
                    <span
                        className={bem.element('label')}
                    >
                        {this.props.icon && this.props.iconPosition === 'left' && (
                            <Icon
                                name={this.props.icon}
                                title={title}
                                className={bem.element('icon', 'is-left')}
                            />
                        )}
                        {this.props.children}
                        {this.props.icon && this.props.iconPosition === 'right' && (
                            <Icon
                                name={this.props.icon}
                                title={title}
                                className={bem.element('icon', 'is-right')}
                            />
                        )}
                    </span>
                )}
            </>
        );
    }

    renderBadge() {
        if (!this.props._badge.enable) {
            return null;
        }

        const bem = this.props.bem;
        return (
            <span className={bem(
                'badge',
                this.props._badge.color && 'badge-' + this.props._badge.color,
                bem.element('badge'),
                this.props._badge.className,
            )}>
                {this.props._badge.value}
            </span>
        );
    }

    _getClassName(modifiers = {}) {
        const bem = this.props.bem;
        return bem(
            bem.block({
                button: !this.props.link,
                link: this.props.link,
                color: this.props.color,
                outline: this.props.outline,
                size: this.props.size,
                disabled: this.props.disabled,
                submitting: this.props.submitting,
                'is-block': this.props.block,
                'is-loading': this.props.isLoading,
                'is-failed': this.props.isFailed,
                'is-button': this.props.linkAsButton,
                ...modifiers,
            }),
            this.props.className,
        );
    }
}
