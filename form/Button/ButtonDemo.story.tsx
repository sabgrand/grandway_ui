import * as React from 'react';
import Button from '@steroidsjs/core/ui/form/Button';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const buttonTypes = [
    'primary',
    'secondary',
    'outline',
    'danger',
    'icon',
    'gradient',
];

const typeStates = [
    'Initial',
    'Disabled',
];

export const Types= () => (
    <StoryPage
        headerTitle='Типы кнопок.'
        headerDescription={[
            __('Задаются при помощи свойства {text}.', {
                text: <span className='story-text-prop'>color</span>
            }),
            __('По-умолчанию: {text}.', {
                text: <span className='story-text-prop'>primary</span>
            }),
        ]}
    >
        {typeStates.map((state, index) => (
            <StorySection
                headerTitle={`Состояние ${state}`}
                key={index}
            >
                {buttonTypes.map((type, index) => {
                    return(
                        <div
                            className='col-2'
                            key={index}
                        >
                            <h6>
                                Тип "{type}"
                            </h6>
                            <Button
                                label={__('Button')}
                                color={type !== 'icon' ? type : 'primary'}
                                icon={type === 'icon' && 'arrow_right'}
                                block
                                disabled={state === 'Disabled'}
                            />
                        </div>
                    )}
                )}
            </StorySection>
        ))}
    </StoryPage>
);

const buttonSizes = [
    'lg',
    'md',
    'sm',
];

export const Sizes = () => (
    <StoryPage
        headerTitle='Размеры кнопок.'
        headerDescription={[
            __('Задаются при помощи свойства {text}.', {
                text: <span className='story-text-prop'>size</span>
            }),
            __('По-умолчанию: {text}.', {
                text: <span className='story-text-prop'>'md'</span>
            })
        ]}
    >
        <StorySection>
            {buttonSizes.map((size, index) => {
                return(
                    <div
                        className='col-3'
                        key={index}
                    >
                        <h6>
                            Размер "{size}"
                        </h6>
                        <Button
                            label={__('Button')}
                            size={size}
                            block
                        />
                    </div>
                )}
            )}
        </StorySection>
    </StoryPage>
);
