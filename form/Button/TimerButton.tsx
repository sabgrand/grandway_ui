import * as React from 'react';
import moment from 'moment';
import padStart from 'lodash-es/padStart';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import {IButtonViewProps} from '@steroidsjs/core/ui/form/Button/Button';


interface ITimerButtonPrivateProps extends IButtonViewProps, IBemHocOutput{
    startingTime?: string,
    padTime?: number,
}

interface ITimerButtonState {
    timerValueSeconds: number,
    timerIsCounting: boolean,
}

@bem('ButtonView')
export default class TimerButton extends React.PureComponent<ITimerButtonPrivateProps, ITimerButtonState> {

    _timer: any;

    constructor(props) {
        super(props);

        const currentTime = moment(moment.utc().format('YYYY-MM-DD HH:mm:ss'));
        const limitTime = moment(this.props.startingTime).add(this.props.padTime, 'minutes');
        const differenceInSeconds =  limitTime.diff(currentTime) / 1000;
        const timeHasEnded = currentTime.isAfter(limitTime);

        this.state = {
            timerValueSeconds: timeHasEnded ? null : differenceInSeconds,
            timerIsCounting: !timeHasEnded,
        };

        this._timer = null;
    }

    componentDidMount() {
        if (this.state.timerIsCounting) {
            this.runTimer();
        }
    }

    componentWillUnmount(){
        if (this._timer) {
            clearInterval(this._timer);
        }
    }

    runTimer() {
        if (this._timer) {
            clearInterval(this._timer);
        }

        this._timer = setInterval(() => {
            if (this.state.timerValueSeconds > 0) {
                this.setState({timerValueSeconds: this.state.timerValueSeconds - 1});
            } else {
                this.setState({timerIsCounting: false});
                clearInterval(this._timer);
            }
        }, 1000);
    }
    
    render() {
        const bem = this.props.bem;
        return (
            <button
                type={this.props.type}
                disabled={this.props.disabled}
                onClick={this.state.timerIsCounting ? null : this.props.onClick}
                style={this.props.style}
                className={this._getClassName()}
            >
                <span className={bem.element('label')}>
                    {this.state.timerIsCounting && (
                        <span>
                            {padStart(Math.trunc(this.state.timerValueSeconds / 60), 2, '0')}
                            :
                            {padStart(Math.round(this.state.timerValueSeconds % 60), 2, '0')}
                        </span>
                    ) || (
                        this.props.label
                    )}
                </span>
            </button>
        );
    }

    _getClassName(modifiers = {}) {
        const bem = this.props.bem;
        return bem(
            bem.block({
                button: !this.props.link,
                link: this.props.link,
                color: this.props.color,
                outline: this.props.outline,
                size: this.props.size,
                disabled: this.state.timerIsCounting,
                submitting: this.props.submitting,
                'is-block': this.props.block,
                'is-loading': this.props.isLoading,
                'is-failed': this.props.isFailed,
                ...modifiers,
            }),
            this.props.className,
        );
    }
}
