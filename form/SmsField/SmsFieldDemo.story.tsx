import * as React from 'react';
import InputField from '@steroidsjs/core/ui/form/InputField';
import SmsFieldView from './SmsFieldView';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const smsStates = [
    'Initial',
    'Disabled',
];

export const Sms= () => (
    <StoryPage
        headerTitle='Поле SmsField'
        headerDescription={[
            'View для InputField - поле предназначено для ввода sms-кодов.',
            __('Для генерации нужного количества цифр кода необходимо указать число {prop}.', {
                prop: <span className='story-text-prop'>maxLength</span>
            })
        ]}
    >
        {smsStates.map((state, index) => (
            <StorySection
                headerTitle={`Состояние ${state}`}
                key={index}
            >
                <div
                    className='col-2'
                    key={index}
                >
                    <InputField
                        attribute='code'
                        label={__('Label')}
                        disabled={state === 'Disabled'}
                        view={SmsFieldView}
                        maxLength={6}
                    />
                </div>
            </StorySection>
        ))}
    </StoryPage>
);