import * as React from 'react';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IInputFieldViewProps} from '@steroidsjs/core/ui/form/InputField/InputField';


interface ISmsFieldViewProps extends IInputFieldViewProps, IBemHocOutput{
    maxLength?: number,
}

@bem('SmsFieldView')
export default class SmsFieldView extends React.PureComponent<ISmsFieldViewProps> {

    render() {
        const bem = this.props.bem;
        return (
            <div
                className={bem(
                    bem.block({
                        disabled: this.props.inputProps.disabled
                    }),
                    this.props.isInvalid && 'is-invalid',
                    this.props.className
                )}
            >
                <input
                    className={bem(
                        bem.element('input'),
                        this.props.isInvalid && 'is-invalid',
                    )}
                    {...this.props.inputProps}
                    onChange={e => this.props.input.onChange(e.target.value)}
                    type={this.props.type}
                    placeholder={this.props.placeholder}
                    disabled={this.props.disabled}
                    maxLength={this.props.maxLength}
                />

                {Array.from(Array(this.props.maxLength)).map((value, index) => {
                    const currentValueChar = this.props.inputProps.value[index];
                    if (currentValueChar) {
                        return (
                            <span
                                key={index}
                                className={bem.element('number-input')}
                            >
                                {currentValueChar}
                            </span>
                        );
                    } else {
                        return (
                            <span
                                key={index}
                                className={bem.element('number-input', 'empty')}
                            >
                                -
                            </span>
                        );
                    }
                })}
            </div>
        );
    }
}
