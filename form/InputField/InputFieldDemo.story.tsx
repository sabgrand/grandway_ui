import * as React from 'react';
import InputField from '@steroidsjs/core/ui/form/InputField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const inputTypes = [
    'standard',
    'finance',
];

const textStates = [
    'Initial',
    'Disabled',
];

export const Text= () => (
    <StoryPage
        headerTitle='Поле InputField'
        headerDescription={[
            'Стандартное поле ввода для формы.'
        ]}
    >
        {textStates.map((type, index) => (
            <StorySection
                headerTitle={`Состояние ${type}`}
                key={index}
            >
                {inputTypes.map((text, index) => (
                    <div
                        className='col-2'
                        key={index}
                    >
                        <h6>
                            Тип "{text}"
                        </h6>
                        <InputField
                            attribute='text'
                            label={__('Label')}
                            addonBefore={text === 'finance' && '$'}
                            disabled={type === 'Disabled'}
                        />
                    </div>
                ))}
            </StorySection>
        ))}
    </StoryPage>
);