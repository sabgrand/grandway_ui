import * as React from 'react';
import DateField from '@steroidsjs/core/ui/form/DateField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const dateStates = [
    'Initial',
    'Filled',
    'Required',
    'Error',
];

export const Date= () => (
    <StoryPage
        headerTitle='Поле DateField.'
        headerDescription={[
            'Поле для выбора даты.'
        ]}
    >
        {dateStates.map((state, index) => (
            <StorySection
                headerTitle={`Состояние ${state}`}
                key={index}
            >
                <div
                    className='col-2'
                    key={index}
                >
                    <DateField
                        attribute='date'
                        label='Дата рождения'
                        input={{
                            value: state === 'Filled' && '24.02.1995'
                        }}
                        required={state === 'Required'}
                        errors={state === 'Error' && 'Ошибка!'}
                    />
                </div>
            </StorySection>
        ))}
    </StoryPage>
);