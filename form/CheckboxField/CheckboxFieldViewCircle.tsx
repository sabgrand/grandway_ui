import * as React from 'react';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {ICheckboxFieldViewProps} from '@steroidsjs/core/ui/form/CheckboxField/CheckboxField';

import './CheckboxFieldViewCircle.scss';

@bem('CheckboxFieldViewCircle')
export default class CheckboxFieldViewCircle extends React.PureComponent<ICheckboxFieldViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(
                bem.block(),
                'custom-control',
                'custom-checkbox'
            )}>
                <div className={bem.element('input-container')}>
                    <input
                        className={bem(
                            bem.element('input'),
                            'custom-control-input',
                            this.props.isInvalid && 'is-invalid',
                            this.props.className
                        )}
                        id={this.props.fieldId + '_' + 'checkbox'}
                        {...this.props.inputProps}
                        disabled={this.props.disabled}
                        required={this.props.required}
                        checked={this.props.inputProps.checked}
                    >
                    </input>
                </div>
                <label
                    className={bem(
                        bem.element('label'),
                        'custom-control-label'
                    )}
                    htmlFor={this.props.fieldId + '_' + 'checkbox'}
                >
                    <span className={bem.element('label-text', {required: this.props.required})}>
                        {this.props.label}
                    </span>
                </label>
            </div>
        );
    }
}
