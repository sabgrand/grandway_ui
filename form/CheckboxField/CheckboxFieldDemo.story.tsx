import * as React from 'react';
import CheckboxField from '@steroidsjs/core/ui/form/CheckboxField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const checkboxTypes = [
    'Checked',
    'Unchecked',
];

const checkboxStates = [
    'Initial',
    'Disabled',
];

export const Default= () => (
    <StoryPage
        headerTitle='Поле CheckboxField.'
        headerDescription={[
            'Стандартный чекбокс.'
        ]}
    >
        {checkboxStates.map((state, index) => (
            <StorySection
                headerTitle={`Состояние ${state}`}
                key={index}
            >
                {checkboxTypes.map((type, index) => (
                    <div
                        className='col-2'
                        key={index}
                    >
                        <h6>
                            Тип {type}
                        </h6>
                        <CheckboxField
                            label='Label'
                            inputProps={{checked: type === 'Checked'}}
                            disabled={state === 'Disabled'}
                        />
                    </div>
                ))}
            </StorySection>
        ))}
    </StoryPage>
);