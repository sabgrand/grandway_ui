import * as React from 'react';
import InputField from '@steroidsjs/core/ui/form/InputField';
import SearchField from './SearchField';
import StoryPage from '../../story-shared/StoryPage';
import StorySection from '../../story-shared/StorySection';

const textStates = [
    'Initial',
    'Filled',
];

export const Search= () => (
    <StoryPage
        headerTitle='Поле SearchField'
        headerDescription={[
            'Поле для поиска.'
        ]}
    >
        {textStates.map((state, index) => (
            <StorySection
                headerTitle={`Состояние ${state}`}
                key={index}
            >
                <div
                    className='col-2'
                >
                    <InputField
                        attribute='search'
                        placeholder='Поиск'
                        inputProps={{
                            value: state === 'Filled' && 'Anton Kirenskiy' || '',
                        }}
                        view={SearchField}
                    />
                </div>
            </StorySection>
        ))}
    </StoryPage>
);