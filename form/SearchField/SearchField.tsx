import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Icon from '@steroidsjs/core/ui/icon/Icon';
import {IInputFieldViewProps} from '@steroidsjs/core/ui/form/InputField/InputField';

@bem('InputFieldView')
export default class InputFieldView extends React.PureComponent<IInputFieldViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div
                className={bem(
                    bem.block({
                        'search-field': true,
                        disabled: this.props.inputProps.disabled,
                        'is-invalid': this.props.isInvalid
                    }),
                    this.props.className
                )}
            >
                <input
                    className={bem.element('input')}
                    {...this.props.inputProps}
                    onChange={e => this.props.input.onChange(e.target.value)}
                    type={this.props.type}
                    value={this.props.input.value}
                    placeholder={this.props.placeholder}
                    disabled={this.props.disabled}
                    required={this.props.required}
                />
                <span className={bem.element('addon-after')}>
                    <Icon name={'search'}/>
                </span>
            </div>
        );
    }

}
