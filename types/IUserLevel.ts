export interface IUserLevel {

    /**
     * ID
     */
    id?: string

    /**
     * Номер уровня
     */
    position?: number

    /**
     * Название
     */
    title?: string

    /**
     * Ставка за единицу
     */
    accrualUsdPerUnit?: number

    /**
     * Оклад
     */
    accrualSalaryUsd?: string

    /**
     * Ставка, %
     */
    accrualPercent?: string

    conditions?: {
        minPersonalUnit?: number,
        minTotalUnit?: number,
    }

    userConditions?: {
        minPersonalUnit?: {
            amount?: number,
            isCompleted?: boolean,
        },
        minTotalUnit?: {
            amount?: number,
            isCompleted?: boolean,
        },
    }

    effects?: {
        name: 'one_time_payment' | 'auto_programm' | 'gift' | string,
        type?: 'phone' | 'tablet' | 'laptop'| 'wristwatch' | string,
        amount?: number,
        label?: string,
    }[],
}
