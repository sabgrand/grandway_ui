import {IUserLevel} from './IUserLevel';
import {IUserStructureParnterProfits} from "./IUserStructureParnterProfits";

export interface IUser {
    accountCode?: string,
    avatarUrl: string,
    city: string,
    countryCode: string,
    createTime: string,
    email: string,
    gender: string,
    grandway?: {
        agreement: any,
        lastPartners:
            {
                avatarUrl: string,
                email: string,
                firstName: string,
                id: number,
                lastName: string,
                middleName: string,
                name: string,
                profitUnit: number,
                profitUsd: number,
                referralCode: string,
            }[],
        partnersCount: number,
    },
    grandwayStatus?: string,
    homewayStatus?: string,
    id: number,
    language?: string,
    leaderReferralCode?: string,
    level: IUserLevel,
    myProfits?: IUserStructureParnterProfits,
    name: string,
    partnersCount: number,
    phone: string,
    profitway: any,
    profitwayStatus?: string,
    referralCode?: string,
    nationalityCode: string,
    social: {
        facebook: string,
        instagram: string,
        telegram: string,
        vk: string,
    }
}