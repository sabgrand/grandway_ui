
export interface IUserStructureParnterProfits  {
    month: [
        {
            amountUnit: string,
            amountUsd: string,
            serviceName: string,
            unitType: string,
        },
    ],
    total: [
        {
            amountUnit: string,
            amountUsd: string,
            serviceName: string,
            unitType: string,
        },
    ],
}
