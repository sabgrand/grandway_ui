/**
 * Элемент галереи, изображение или видео
 */
export default interface IGalleryItem {
    full: {
        height: number,
        url: string,
        width: number,
    },
    thumbnail: {
        height: number,
        url: string,
        width: number,
    },
    type: 'image' | 'video',
    videoUrl?: string,
    videoId?: string,
}
