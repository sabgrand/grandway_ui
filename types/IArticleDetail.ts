import {IArticle} from './IArticle';
import IGalleryItem from './IGalleryItem';

/**
 * Новостная статья, полное описание
 */
export default interface IArticleDetail extends IArticle {

    /**
     * Контент статьи (html)
     */
    content: string,

    /**
     * Массив элементов (картинок и видео) для галереи
     */
    galleryItems?: IGalleryItem[],
}
