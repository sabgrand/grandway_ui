/**
 * Новостная статья, краткое описание
 */
export interface IArticle {
    /**
     * ID
     */
    id: number,

    /**
     * Название
     */
    title: string,

    /**
     * Описание
     */
    description: string,

    /**
     * Время создания
     */
    createTime: string,

    /**
     * Время обновления
     */
    updateTime: string,

    /**
     * Имя латиницей (slug)
     */
    name: string,

    /**
     * Ссылка на видео
     */
    resourceUrl?: string,

    /**
     * Категория статьи
     */
    category: {

        /**
         * Id категории
         */
        name: string,

        /**
         * Название категории
         */
        title: string,
    },

    /**
     * Ссылка на обрезанное превью статьи
     */
    imageThumbnailUrl: string,

    /**
     * Ссылка на оригинальное превью статьи
     */
    imageFullUrl: string,

    /**
     * Данные документа для скачивания
     */
    material?: {
        id: number,
        title: string,
        url: string,

        downloadUrl: string,
        fileMimeType: string,
        fileName: string,
        fileSize: number,
        folder: string,
        images: any[],

        createTime: string,
        md5: string,
        uid: string,
        userId: string,
    }
}
