import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

interface ILoaderViewProps extends IBemHocOutput {
}

import './LoaderView.scss';

@bem('LoaderView')
export default class LoaderView extends React.PureComponent<ILoaderViewProps> {
    render() {
        const bem = this.props.bem;

        return (
            <div className={bem.block()}>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        );
    }
}