import * as React from 'react';
import {showNotification} from '@steroidsjs/core/actions/notifications'

import Button from '@steroidsjs/core/ui/form/Button';

export default {
    title: 'Layout/Notification',
}

const notifications = {
    primary: 'Primary notification',
    secondary: 'Secondary notification',
    success: 'Success notification',
    danger: 'Danger notification',
    warning: 'Warning notification',
    info: 'Info notification',
    light: 'Light notification',
    dark: 'Dark notification',
};

export const Types = (props) => {
    return(
        <>
            {Object.keys(notifications).map((level: string) => (
                [].concat(notifications[level] || [])
                    .map(message => (
                        <Button
                            key={level}
                            color={level}
                            label={__(`Уведомление типа "${level}"`)}
                            // onClick={() => dispatch(showNotification(message, level))}
                            style={{margin: '10px 0'}}
                            block
                        />
                    ))
            ))}
        </>
    );

}