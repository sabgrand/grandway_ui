import * as React from 'react';

import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {INotificationsItemViewProps} from '@steroidsjs/core/ui/layout/Notifications/Notifications';
import {CSSTransition} from 'react-transition-group';
import Icon from '@steroidsjs/core/ui/icon/Icon';

interface INotificationsState {
    isShow: boolean
}

@bem('NotificationsItemView')
export default class NotificationsItemView extends React.Component<INotificationsItemViewProps & IBemHocOutput, INotificationsState> {
    constructor(props) {
        super(props);

        this.state = {
            isShow: false,
        };
    }

    componentDidMount(): void {
        this.setState({isShow: true});
    }

    componentDidUpdate(prevProps: INotificationsItemViewProps & IBemHocOutput): void {
        if (prevProps.isClosing !== this.props.isClosing) {
            this.setState({isShow: !this.props.isClosing});
        }
    }

    render() {
        const bem = this.props.bem;
        return (
            <CSSTransition
                in={this.state.isShow}
                timeout={1000}
                classNames={bem(bem.block({
                    [this.props.position]: true,
                }))}
                unmountOnExit
            >
                <div
                    className={bem(
                        bem.block({
                            [this.props.level]: true,
                            [this.props.position]: true,
                        }),
                    )}
                >
                    <div className={bem.element('content-container')}>
                        {this.props.level === 'success' && (
                            <Icon
                                className={bem.element('success-icon')}
                                name='success_mark'
                            />
                        )}
                        <span className={bem.element('message')}>
                            {this.props.message}
                        </span>
                    </div>
                    <div
                        className={bem.element('close')}
                        onClick={this.props.onClose}
                    >
                        <Icon
                            className={bem.element('close-icon')}
                            name='x_close'
                        />
                    </div>
                </div>
            </CSSTransition>
        );
    }
}
