import * as React from 'react';
import {RefObject} from 'react';
import _max from 'lodash-es/max';
import _min from 'lodash-es/min';
import _round from 'lodash-es/round';
import _padEnd from 'lodash-es/padEnd';
import {bem, components} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IConnectHocOutput} from '@steroidsjs/core/hoc/connect';

import {
    FlexibleWidthXYPlot,
    GradientDefs,
    HorizontalGridLines,
    XAxis,
    YAxis,
    LineSeries,
    MarkSeries,
    AreaSeries,
    VerticalBarSeries,
    LabelSeries,
    Hint,
} from 'react-vis';

import {moneyFormat} from '@steroidsjs/core/ui/format/MoneyFormatter/MoneyFormatter';
import Empty from '../../display/Empty';
import LegendItem from '../LegendItem';
import DataSelectionPanel from '../DataSelectionPanel';

import GraphColors from '../../utilites/enum/GraphColors';
import CurrencyEnum from '../../utilites/enum/CurrencyEnum';

import 'react-vis/dist/style.css';
import './ProfitGraph.scss';

export interface ISeriesStateItem {
    label?: string,
    symbol?: string,
    points?: {
        x: string,
        y: number,
        additionalData: number,
    }[],
    color?: string,
}

interface IProfitGraphProps extends IBemHocOutput, IConnectHocOutput {
    series?: any,
    isSingleGraph?: boolean,
    graphHeight?: number,
    periods?: {
        id: string,
        label: string,
    }[],
    selectedPeriod?: string,
    onPeriodSelect?: (id: any) => void,
    emptyText?: string,
    graphType?: 'profitway' | 'verticalBar',
    amountCount?: number,
    headerText?: string,
    hideLegend?: boolean,
    byAllTime?: boolean,
    noneDateType?: boolean,
    toFixed?: number,
    hideMarks?: boolean,
    labelGetHigh?: boolean,
    scale?: number
}

interface IProfitGraphState {
    series?: ISeriesStateItem[],
    isEmpty?: boolean,
    maxLineLength: number,
    hasValues: boolean,
    hasAdditionalData: boolean,
    maxYValue: number,
    hintValue: {
        x: string,
        y: number,
    },
    hintData?: any,
    maxXAxisLength?: number,
    scale?: number
}

let ID_COUNTER = 0;
const BASE_LEFT_MARGIN = 25;
const DIGIT_LENGTH = 7;

@components('locale')
@bem('ProfitGraph')
export default class ProfitGraph extends React.PureComponent<IProfitGraphProps, IProfitGraphState> {
    static defaultProps = {
        series: [],
        graphHeight: 150,
        isSingleGraph: true,
        amountCount: 5,
        headerText: __('График прибыли'),
        hideMarks: false,
        labelGetHigh: false,
        scale: 0,
    };

    _id: number;

    _rootRef: RefObject<any>;

    constructor(props) {
        super(props);

        this._id = ++ID_COUNTER;
        this._rootRef = React.createRef();

        this.state = {
            series: [],
            isEmpty: true,
            hintValue: null,
            maxYValue: 0,
            hasValues: false,
            hasAdditionalData: false,
            maxLineLength: 0,
            maxXAxisLength: null,
        };
    }

    componentDidMount() {
        if (this.props.series.filter(seriesItem => seriesItem.points.length > 0).length !== 0) {
            this.loadGraphData();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.series !== this.props.series) {
            if (this.props.series.filter(seriesItem => seriesItem.points.length > 0).length === 0) {
                this.setState({isEmpty: true});
            } else {
                this.loadGraphData();
            }
        }
    }

    loadGraphData() {
        const values = this.props.series.filter(item => item.points.length > 0).map(item => item.points.map(point => point[1])).flat(2);
        const xValues = this.props.series.filter(item => item.points.length > 0).map(item => item.points.map(point => point[0])).flat(2);

        const mavValues = _max(xValues);
        const minValues = _min(xValues);

        if (this.props.noneDateType) {
            this.setState({
                ...this.props.series,
                series: this.props.series.map(item => (
                    {
                        ...item,
                        points: item.points.map((point, index) => ({
                            x: point[0],
                            y: point[1],
                            additionalData: 0,
                            index: index + 1,
                        })),
                    }
                )),
                isEmpty: false,
                maxYValue: _round(_max(values)),
                hasValues: true,
            });

            return;
        }

        const unitOfTime = this.props.byAllTime ? 'year' : 'months';
        const formatDisplayTime = unitOfTime === 'year' ? 'YYYY' : 'MMM YYYY';

        const differenceBetweenMaxAndMin = this.props.locale.moment(mavValues).diff(this.props.locale.moment(minValues), unitOfTime) + 1;

        const templateXAxis = new Array(this.props.amountCount).fill([])
            .map((item, index) => (
                differenceBetweenMaxAndMin > this.props.amountCount
                    ? [...item, this.props.locale.moment(mavValues).subtract(index, unitOfTime)]
                    : [...item, this.props.locale.moment(minValues).add(index, unitOfTime)]
            ));

        if (differenceBetweenMaxAndMin > this.props.amountCount) {
            templateXAxis.reverse();
        }

        let hasAdditionalData = false;
        this.setState({
            series: this.props.series.map(item => (
                {
                    ...item,
                    points: templateXAxis.map((XAxis, index) => ({
                        x: XAxis[0].format(formatDisplayTime),
                        y: item.points.reduce((accumulator, point) => {
                            if (this.props.locale.moment(point[0]).isSame(XAxis[0])) {
                                accumulator = point[1];
                            }
                            return accumulator;
                        }, 0),
                        additionalData: item.points.reduce((accumulator, point) => {
                            if (!!point[2] && this.props.locale.moment(point[0]).isSame(XAxis[0])) {
                                accumulator = moneyFormat(point[2], CurrencyEnum.RUB, 0);
                                hasAdditionalData = true;
                            }
                            return accumulator;
                        }, 0),
                        index: index + 1,
                    })),
                }
            )),
            isEmpty: false,
            maxYValue: _round(_max(values)),
            hasValues: values.length > 0,
            hasAdditionalData,
            maxXAxisLength: differenceBetweenMaxAndMin,
        });
        this._refreshMaxLineLength();
    }

    onValueMouseOver = value => {
        if (this.state.hintValue === null) {
            this.loadHintData(value);
        } else if (this.state.hintValue.x !== value.x) {
            this.loadHintData(value);
        }
    };

    onValueMouseOut = () => {
        this.setState({
            hintValue: null,
        });
    };

    loadHintData(value) {
        if (this.props.isSingleGraph) {
            this.setState({hintValue: value});
        } else {
            const data = [];
            this.state.series.map(seriesItem => {
                const selectedData = seriesItem.points.find(item => item.y === value.y && item.x === value.x
                    || value.y - ((this.state.maxYValue / 100) * 5) <= item.y && item.y <= value.y + ((this.state.maxYValue / 100) * 5) && item.x === value.x);
                if (selectedData) {
                    data.push({
                        data: CurrencyEnum.format(selectedData.y, '', true, this.props.scale),
                        additionalData: selectedData.additionalData || '0 $',
                        color: seriesItem.color,
                        label: seriesItem.symbol || seriesItem.label,
                    });
                }
            });
            this.setState({hintValue: value, hintData: data});
        }
    }

    render() {
        const bem = this.props.bem;
        return (
            <div
                ref={this._rootRef}
                className={bem.block()}
            >
                {(!this.props.isSingleGraph) && (
                    <div className={bem.element('control-panel')}>
                        {this.renderHeader()}
                    </div>
                )}
                {this.state.isEmpty ? this.renderEmpty() : this.renderGraph()}
            </div>
        );
    }

    renderHeader() {
        const bem = this.props.bem;
        if (this.props.periods) {
            return (
                <>
                    {this.props.hideLegend ? null : this.renderLegend()}
                    <DataSelectionPanel
                        selected={this.props.selectedPeriod}
                        items={this.props.periods}
                        onSelect={this.props.onPeriodSelect}
                        className={this.props?.graphType}
                    />
                </>
            );
        }

        return (
            this.props.hideLegend ? null : (
                <>
                    <span className={bem.element('header')}>
                        {this.props.headerText}
                    </span>
                    {this.renderLegend()}
                </>
            )
        );
    }

    renderLegend() {
        const bem = this.props.bem;
        return (
            <div className={bem.element('legend-container')}>
                {this.props.series.map((seriesItem, index) => {
                    let color;
                    if (this.state.isEmpty) {
                        color = index % 2 === 0 ? GraphColors.EMPTY_BLUE_LIGHT : GraphColors.EMPTY_BLUE;
                    } else {
                        color = seriesItem.color;
                    }
                    return (
                        <LegendItem
                            key={index}
                            color={color}
                            label={seriesItem.label}
                        />
                    );
                })}
            </div>
        );
    }

    renderGraph() {
        const bem = this.props.bem;

        const digits = Number(_padEnd('1', this.state.maxYValue.toString().length, '0'));
        const maxYDomain = Math.ceil(this.state.maxYValue / digits + 0.5) * digits;
        const calcMargin = (String(digits).length - 1) * DIGIT_LENGTH;

        return (
            <FlexibleWidthXYPlot
                height={this.props.graphHeight}
                margin={this.props.isSingleGraph ? {left: 0, right: 40} : {left: BASE_LEFT_MARGIN + calcMargin}}
                yDomain={[0, maxYDomain]}
                xType='ordinal'
                dontCheckIfEmpty
            >
                <GradientDefs>
                    <linearGradient id={`LinearGradient_${this._id}`} x1='0' x2='0' y1='0' y2='1'>
                        <stop
                            offset='10%'
                            stopColor={GraphColors.getLabel(this.props.series[0].color)}
                            stopOpacity={0.4}
                        >
                            <animate attributeName='stop-opacity' values='0; .1; .2; .3; .4' dur='5s' />
                        </stop>
                        <stop offset='90%' stopColor='#fff' stopOpacity={0.25} />
                    </linearGradient>
                    <filter x='-0.25' y='-0.25' width='1.5' height='1.5' rx='10'>
                        <feFlood floodColor='#14A96A' />
                        <feComposite in='SourceGraphic' operator='xor' />
                    </filter>
                </GradientDefs>
                <HorizontalGridLines
                    tickTotal={this.props.isSingleGraph ? 3 : 6}
                    style={{strokeDasharray: '4'}}
                />
                <XAxis
                    hideLine
                    style={{
                        stroke: 'transparent',
                        fill: 'rgba(62,62,62,.4)',
                        fontSize: '10px',
                    }}
                    tickTotal={2}
                    tickSize={8}
                    tickSizeOuter={2}
                    hideTicks={!this.state.hasValues}
                    tickFormat={wordsString => {
                        const wordsArray = wordsString.split(' ');
                        return this.props.noneDateType
                            ? (
                                <tspan>
                                    <tspan x='0' dy='1em'>
                                        {wordsArray.slice(0, Math.round(wordsArray.length / 2)).join(' ')}
                                    </tspan>
                                    <tspan x='0' dy='1em'>
                                        {wordsArray.slice(Math.round(wordsArray.length / 2)).join(' ')}
                                    </tspan>
                                </tspan>
                            )
                            : wordsArray;
                    }}
                />
                <YAxis
                    orientation={this.props.isSingleGraph ? 'right' : 'left'}
                    tickTotal={this.props.isSingleGraph ? 3 : 6}
                    style={{
                        stroke: 'transparent',
                        fill: 'rgba(62,62,62,.4)',
                        fontSize: '10px',
                    }}
                />
                {this.props.isSingleGraph && (
                    <AreaSeries
                        className={bem.element('gradient-area')}
                        data={this.state.series[0].points}
                        animation={false}
                        curve='curveCardinal'
                        color={`url(#LinearGradient_${this._id})`}
                        stroke="transparent"
                        style={{opacity: '0'}}
                    />
                )}

                {this.props.graphType !== 'verticalBar' && (
                    this.state.series.map((item, index) => (
                        <LineSeries
                            key={index}
                            data={item.points}
                            animation={false}
                            className={bem.element('graph', {
                                [item.color]: true,
                            })}
                            curve={this.props.isSingleGraph ? 'curveCardinal' : 'curveLinear'}
                            color={GraphColors.getLabel(item.color)}
                            style={{
                                strokeDasharray: this.state.maxLineLength,
                                strokeDashoffset: this.state.maxLineLength,
                                opacity: this.state.maxLineLength === 0 ? 0 : 1,
                            }}
                            getNull={item => item.index <= this.state.maxXAxisLength}
                        />
                    )))}

                {(this.props.graphType === 'profitway' || this.props.graphType === 'verticalBar') && (
                    this.state.series.map((item, index) => (
                        <VerticalBarSeries
                            key={index}
                            data={this.props.noneDateType
                                ? item.points
                                : [...item.points.map(item => (
                                    {
                                        ...item,
                                        y: this.props.graphType === 'verticalBar' ? item.y : item.y / 2,
                                    }
                                ))]}
                            barWidth={0.6}
                            style={{fill: '#742AEE', rx: 5, stroke: null}}
                        />
                    ))
                )}

                {(this.props.graphType === 'profitway' || this.props.graphType === 'verticalBar') && (
                    <LabelSeries
                        data={this.props.labelGetHigh
                            ? this.state.series[0].points.map(item => ({...item, y: item.y / 2}))
                            : this.state.series[0].points.map(item => ({...item, y: item.y / 4}))}
                        getLabel={(item) => {
                            if (item.y === 0) {
                                return '';
                            }
                            if (this.props.labelGetHigh) {
                                return (item.y * 2).toFixed(this.props.toFixed || 1) + '%';
                            }
                            return (item.y * 4).toFixed(this.props.toFixed || 1) + '%';
                        }}
                        labelAnchorX="middle"
                        labelAnchorY="central "
                        style={{fill: 'white'}}
                    />
                )}
                {!this.props.hideMarks && this.state.series.map((item, index) => (
                    <MarkSeries
                        key={index}
                        data={item.points}
                        animation={false}
                        className={bem.element('mark', {
                            [item.color]: true,
                        })}
                        stroke={GraphColors.getLabel(item.color)}
                        fill='white'
                        strokeWidth={2}
                        onValueMouseOver={this.onValueMouseOver}
                        onValueMouseOut={this.onValueMouseOut}
                        getNull={item => item.index <= this.state.maxXAxisLength}
                    />
                ))}
                {this.state.hintValue && (this.props.isSingleGraph && (
                    <Hint
                        className={bem.element('hint-black')}
                        value={this.state.hintValue}
                        align={{horizontal: 'auto', vertical: 'top'}}
                    >
                        <span className={bem.element('hint-black-content')}>
                            {this.state.hintValue.y}
                        </span>
                    </Hint>
                ) || (
                    <Hint
                        className={bem.element('hint-blurred')}
                        value={this.state.hintValue}
                        align={{horizontal: 'auto', vertical: 'auto'}}
                    >
                        <span className={bem.element('hint-blurred-content')}>
                            <div className={bem.element('hint-blurred-column')}>
                                {this.state.hintData.map((item, index) => (
                                    <span
                                        key={index}
                                        className={bem.element('hint-item-value', {[item.color]: true})}
                                    >
                                        {item.data}
                                        {' '}
                                        {item.label}
                                    </span>
                                ))}
                            </div>
                            {this.state.hasAdditionalData && (
                                <div className={bem.element('hint-blurred-column')}>
                                    {this.state.hintData.filter(item => item.additionalData).map((item, index) => (
                                        <span
                                            key={index}
                                            className={bem.element('hint-item-value')}
                                        >
                                            {item.additionalData}
                                        </span>
                                    ))}
                                </div>
                            )}
                        </span>
                    </Hint>
                ))}
            </FlexibleWidthXYPlot>
        );
    }

    renderEmpty() {
        const bem = this.props.bem;
        return (
            <>
                <FlexibleWidthXYPlot
                    height={this.props.graphHeight}
                    margin={this.props.isSingleGraph ? {left: 0, right: 40} : {left: 40}}
                    yDomain={[0, this.state.maxYValue || 100]}
                    xType='ordinal'
                    dontCheckIfEmpty
                >
                    <YAxis
                        orientation={this.props.isSingleGraph ? 'right' : 'left'}
                        tickTotal={this.props.isSingleGraph ? 3 : 6}
                        style={{
                            stroke: 'transparent',
                            fill: 'rgba(62,62,62,.4)',
                            fontSize: '10px',
                        }}
                    />
                    <HorizontalGridLines
                        tickTotal={6}
                        style={{strokeDasharray: '4'}}
                    />
                </FlexibleWidthXYPlot>
                <Empty
                    className={bem.element('empty-container')}
                    label={this.props.emptyText || __('Нет данных')}
                />
            </>
        );
    }

    _refreshMaxLineLength() {
        setTimeout(() => {
            let maxLineLength = 0;
            if (this._rootRef.current) {
                const elements = this._rootRef.current.querySelectorAll('.rv-xy-plot__series--line');
                elements.forEach((element: SVGGeometryElement) => {
                    maxLineLength = Math.max(maxLineLength, element.getTotalLength());
                });
                if (this.state.maxLineLength !== maxLineLength) {
                    this.setState({maxLineLength});
                }
            }
        });
    }
}
