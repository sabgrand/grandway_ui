import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import BalanceRadialGraph from '../BalanceRadialGraph';
import BalanceProgressView from '../BalanceRadialGraph/views/BalanceProgressView';

import {IGraph} from '../../utilites/enum/IGraph';
import CurrencyEnum from '../../utilites/enum/CurrencyEnum';

import './PwReportGraph.scss';

interface IPwReportGraphProps extends IBemHocOutput {
    investmentItem: IGraph
}

@bem('PwReportGraph')
export default class PwReportGraph extends React.PureComponent<IPwReportGraphProps> {
    render() {
        const bem = this.props.bem;

        const investmentItem = this.props.investmentItem;
        let resultGraphData: Object;
        let resultGraphItems: any[];

        if (investmentItem.subAllocations.length !== 0) {
            resultGraphData = investmentItem.subAllocations.reduce((accumulator, item) => {
                accumulator[item.area.label] = Number(item.percent);
                return accumulator;
            }, {});

            resultGraphItems = investmentItem.subAllocations.map(item => ({
                attribute: item.area.label,
                label: item.area.label,
                color: item.area.color,
            }));
        } else {
            resultGraphData = {
                [investmentItem.area.label]: investmentItem.percent,
            };

            resultGraphItems = [{
                attribute: investmentItem.area.label,
                label: investmentItem.area.label,
                color: investmentItem.area.color,
            }];
        }

        return (
            <div className={bem.block()}>
                <h2 className={bem.element('section-header')}>
                    {investmentItem.area.label}
                    <span>
                        {` - ${investmentItem.percent}%`}
                    </span>
                </h2>
                <BalanceRadialGraph
                    className={bem.element('radial-graph-container')}
                    url=''
                    graphData={{...resultGraphData}}
                    progressViewItem={BalanceProgressView}
                    progressViewItemType='column'
                    items={resultGraphItems}
                    currency={CurrencyEnum.PERCENT}
                    graphTotalTemplate={investmentItem.percent + '%'}
                    loadManualData
                    hideSelectionPanel
                    showFullValueInHint
                />
            </div>
        );
    }
}
