import * as React from 'react';
import {findDOMNode} from 'react-dom';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import {RadialChart} from 'react-vis';
import LegendItem from '../LegendItem';

import GraphColors from '../../utilites/enum/GraphColors';

import './ProfitRadial.scss';

interface IProfitRadialArrayItem extends IBemHocOutput {
    attribute: string,
    theta: number,
    label: string,
    value?: number,
    legendLabel: string,
    color: string,
}

interface IProfitRadialProps extends IBemHocOutput {
    graphArray: IProfitRadialArrayItem[],
    totalSum: string,
    showLegend?: boolean,
    isEmpty?: boolean,
    className?: CssClassName,
    radialSize?: number,
    currency?: string,
    scale?: number,
}

interface IProfitRadialState {
    hintArray: any,
}

@bem('ProfitRadial')
export default class ProfitRadial extends React.PureComponent<IProfitRadialProps, IProfitRadialState> {
    static defaultProps = {
        showLegend: false,
        isEmpty: true,
        radialSize: 190,
    }

    constructor(props) {
        super(props);
        this.state = {
            hintArray: null,
        };
    }

    componentDidMount() {
        const graphLabels = findDOMNode(this).querySelectorAll('.rv-xy-plot__series--label-text');
        if (graphLabels.length > 0) {
            this.loadHintArray(graphLabels);
        }
    }

    componentDidUpdate() {
        if (!this.props.isEmpty && !this.state.hintArray) {
            const graphLabels = findDOMNode(this).querySelectorAll('.rv-xy-plot__series--label-text');
            this.loadHintArray(graphLabels);
        }
    }

    loadHintArray(graphLabels) {
        const hintArray = [];
        graphLabels.forEach((section, index) => {
            const labelRect = graphLabels[index].getBoundingClientRect();
            const hintX = Math.abs(parseInt(graphLabels[index].getAttribute('x')) + (labelRect.width / 2));
            const hintY = Math.abs(parseInt(graphLabels[index].getAttribute('y')) + (labelRect.height / 2));

            hintArray.push({
                x: hintX,
                y: hintY,
            });
        });
        this.setState({hintArray});
    }

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                <div
                    className={bem.element('graph-container')}
                    style={{
                        width: this.props.radialSize + 10,
                        height: this.props.radialSize + 10,
                    }}
                >
                    <RadialChart
                        className={bem.element('graph')}
                        width={this.props.radialSize}
                        height={this.props.radialSize}
                        colorType='literal'
                        data={this.props.graphArray.map(item => ({
                            ...item,
                            color: item.theta === 0 ? 'transparent' : (GraphColors.getLabel(item.color) || item.color),
                        }))}
                        radius={this.props.radialSize / 2 - 1}
                        innerRadius={this.props.radialSize / 2 - 40}
                        getAngle={d => d.theta}
                        padAngle={0.04}
                        labelsRadiusMultiplier={1}
                        showLabels={!this.props.isEmpty}
                        labelsStyle={{
                            fill: 'transparent',
                        }}
                    />
                    {!this.props.isEmpty && this.state.hintArray?.length > 1 && (this.state.hintArray || [])
                        .map((hint, index) => {
                            if (this.props.graphArray[this.props.graphArray.length - 1 - index].theta === 0) {
                                return null;
                            }
                            return (
                                <div
                                    key={index}
                                    className={bem.element('hint')}
                                    style={{left: hint.x, top: hint.y}}
                                >
                                    {this.props.graphArray[this.props.graphArray.length - 1 - index].label}
                                </div>
                            );
                        })}
                    <div className={bem.element('total-value', {empty: this.props.isEmpty})}>
                        {this.props.isEmpty ? __('Дохода пока нет') : this.props.totalSum}
                    </div>
                </div>
                {this.props.showLegend && (
                    <div className={bem.element('legends-container')}>
                        {this.props.graphArray.map((item, index) => (
                            <LegendItem
                                key={index}
                                color={item.color}
                                label={item.legendLabel || item.label}
                                value={item.value}
                                currency={this.props.currency}
                                scale={this.props.scale}
                            />
                        ))}
                    </div>
                )}
            </div>
        );
    }
}
