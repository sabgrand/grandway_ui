import * as React from 'react';
import {bem, components, fetch} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IFetchHocOutput} from '@steroidsjs/core/hoc/fetch';
import {IComponentsHocOutput} from '@steroidsjs/core/hoc/components';

import ProfitBlock from '../ProfitBlock';
import ProfitGraph from '../ProfitGraph';

import GraphColors from '../../utilites/enum/GraphColors';

import './StatisticGraphItem.scss';


interface IGwStatisticItem {
    all: number,
    lastMonth: number,
    prevMonth: number,
    lastWeek: number,
    prevWeek: number,
    points: [],
    scale: number
}

interface IStatisticGraphItemProps extends IBemHocOutput, IComponentsHocOutput, IFetchHocOutput {
    title?: string,
    url: string,
    personalLabel?: string,
    structureLabel?: string,
    monthBlockLabel?: string,
    allBlockLabel?: string,
    personalLineLabel?: string,
    structureLineLabel?: string,
    showMode?: 'profitway',
    statistic?: {
        personal: IGwStatisticItem,
        structure: IGwStatisticItem,
    },
    headerText?: string,
    className?: CssClassName,
    scale: number
}

@fetch(
    (props) => {
        return {
            id: `StatisticGraphItem_${props.url}`,
            url: props.url,
            key: 'statistic',
        };
    },
    {
        waitLoading: false,
    }
)
@components('locale')
@bem('StatisticGraphItem')
export default class StatisticGraphItem extends React.PureComponent<IStatisticGraphItemProps> {

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                <h2 className={bem.element('header')}>
                    {this.props.title}
                </h2>
                <div className={bem(bem.element('statistics-container', {[this.props?.showMode]: true}), 'row')}>
                    {this.props.showMode === 'profitway' && (
                        this.renderMiniGraph()
                    ) || (
                        this.renderFullsizeGraph()
                    )}
                </div>
            </div>
        );
    }

    renderFullsizeGraph() {
        const bem = this.props.bem;

        return (
            <>
                <div className={bem(bem.element('statistics-value-container'), 'col-3')}>
                    <ProfitBlock
                        title={this.props.monthBlockLabel}
                        data={[
                            {
                                label: this.props.personalLabel,
                                value: this.props.statistic
                                    ? this.props.statistic.personal.lastMonth
                                    : null,
                            },
                            {
                                label: this.props.structureLabel,
                                value: this.props.statistic
                                    ? this.props.statistic.structure.lastMonth
                                    : null,
                            },
                            {
                                label: __('Всего'),
                                value: this.props.statistic
                                    ? this.props.statistic.personal.lastMonth + this.props.statistic.structure.lastMonth
                                    : null
                            },
                        ]}
                    />
                    <ProfitBlock
                        title={this.props.allBlockLabel}
                        data={[
                            {
                                label: this.props.personalLabel,
                                value: this.props.statistic
                                    ? this.props.statistic.personal.all
                                    : null,
                            },
                            {
                                label: this.props.structureLabel,
                                value: this.props.statistic
                                    ? this.props.statistic.structure.all
                                    : null,
                            },
                            {
                                label: __('Всего'),
                                value: this.props.statistic
                                    ? this.props.statistic.personal.all + this.props.statistic.structure.all
                                    : null,
                            },
                        ]}
                    />
                </div>
                <div className={bem(bem.element('statistics-graph-container'), 'col-9')}>
                    <ProfitGraph
                        key={`ProfitGraph_${this.props.url}`}
                        graphHeight={200}
                        series={[
                            {
                                label: this.props.personalLineLabel,
                                color: GraphColors.BLUE,
                                points: this.props.statistic?.personal?.points || [],
                            },
                            {
                                label: this.props.structureLineLabel,
                                color: GraphColors.YELLOW,
                                points: this.props.statistic?.structure?.points || [],
                            },
                        ]}
                        amountCount={12}
                        isSingleGraph={false}
                        headerText={this.props.headerText}
                        scale={this.props.scale}
                    />
                </div>
            </>
        );
    }

    renderMiniGraph() {
        const bem = this.props.bem;
        return (
            <>
                <div className={bem.element('statistics-value-container', {[this.props?.showMode]: true})}>
                    <ProfitBlock
                        title={this.props.allBlockLabel}
                        className={this.props?.showMode}
                        data={[
                            {
                                label: this.props.personalLabel,
                                value: this.props.statistic
                                    ? this.props.statistic.personal.all
                                    : null,
                            },
                            {
                                label: this.props.structureLabel,
                                value: this.props.statistic
                                    ? this.props.statistic.structure.all
                                    : null,
                            },
                            {
                                label: __('Итого'),
                                value: this.props.statistic
                                    ? this.props.statistic.personal.all + this.props.statistic.structure.all
                                    : null,
                            },
                        ]}
                    />
                </div>
                <div className={bem(bem.element('statistics-graph-container'))}>
                    <ProfitGraph
                        key={`ProfitGraph_${this.props.url}`}
                        graphHeight={200}
                        series={[
                            {
                                label: this.props.personalLineLabel,
                                color: GraphColors.GREEN,
                                points: this.props.statistic?.personal?.points || [],
                            },
                        ]}
                        isSingleGraph={false}
                        graphType={this.props?.showMode}
                        amountCount={14}
                        scale={this.props.scale}
                    />
                </div>
            </>
        );
    }
}
