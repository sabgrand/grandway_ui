import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import CurrencyEnum from '../../utilites/enum/CurrencyEnum';

import './LegendItem.scss';

interface ILegendItemProps extends IBemHocOutput {
    color?: string,
    label?: string,
    value?: number | null,
    currency?: string,
    scale?: number,
}

@bem('LegendItem')
export default class LegendItem extends React.PureComponent<ILegendItemProps> {
    render() {
        const bem = this.props.bem;
        const showValue = this.props.value >= 0 || this.props.value === null;
        const style = {'--legend-color': this.props.color} as React.CSSProperties;
        return (
            <div className={bem.block({})}>
                <div
                    className={bem.element('color-block')}
                    style={style}
                />
                <div className={bem.element('body')}>
                    <span className={bem.element('label', {'with-value': showValue})}>
                        {this.props.label}
                    </span>
                    {showValue && (
                        <span className={bem.element('value')}>
                            {this.props.value
                                ? (
                                    this.props.currency === CurrencyEnum.USD
                                    ? CurrencyEnum.formatUsd(this.props.value)
                                    : CurrencyEnum.format(this.props.value, '', true, this.props.scale)

                                )
                                : '-'}
                        </span>
                    )}
                </div>
            </div>
        );
    }
}
