import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Icon from '@steroidsjs/core/ui/icon/Icon';
import DataProgressBar from '../../../../display/DataProgressBar';
import Tooltip from '@steroidsjs/core/ui/layout/Tooltip';

import './BalanceProgressBarItem.scss';


interface IBalanceProgressBarItemProps extends IBemHocOutput {
    label: string,
    value: number | string,
    color: string,
    percentage: number,
    className?: CssClassName,
    helpText?: string
}

@bem('BalanceProgressBarItem')
export default class BalanceProgressBarItem extends React.PureComponent<IBalanceProgressBarItemProps> {

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                <div className={bem.element('header')}>
                    <span className={bem.element('label')}>
                        {this.props.label}
                        {this.props.helpText && (
                            <Tooltip position={'top'} content={this.props.helpText}>
                                <div className={bem.element('icon-container')}>
                                    <Icon name={'help'}/>
                                </div>
                            </Tooltip>
                        )}
                    </span>
                    <span className={bem.element('value')}>
                        {this.props.value}
                    </span>
                </div>
                <DataProgressBar
                    fillPercentage={this.props.percentage}
                    color={this.props.color}
                />
            </div>
        );
    }
}
