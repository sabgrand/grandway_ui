import * as React from 'react';
import _round from 'lodash-es/round';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import GraphColors from '../../../../utilites/enum/GraphColors';
import CurrencyEnum from '../../../../utilites/enum/CurrencyEnum';
import BalanceProgressBarItem from '../BalanceProgressBarItem';

import './BalanceProgressView.scss';


interface IBalanceProgressViewProps extends IBemHocOutput {
    balanceData: any,
    totalValue: number,
    currency?: string,
    scale?: number,

    viewType?: 'row' | 'column',
    viewSize?: 'lg' | 'md' | 'sm',
}

@bem('BalanceProgressView')
export default class BalanceProgressView extends React.PureComponent<IBalanceProgressViewProps> {

    static defaultProps = {
        viewType: 'row',
        viewSize: 'md',
        scale: 0,
    }

    render() {
        const bem = this.props.bem;
        return (
            <div 
                className={bem.block({
                    [this.props.viewType]: true,
                    [this.props.viewSize]: true,
                })}
            >
                {this.props.balanceData.map(item => (
                    <BalanceProgressBarItem
                        key={item.attribute}
                        label={item.label}
                        value={this.props.totalValue === 0
                            ? 0
                            : (this.props.scale
                                ? CurrencyEnum.format(
                                    item.value,
                                    this.props.currency
                                        ? this.props.currency
                                        : '',
                                    true,
                                    this.props.scale
                                ) : _round(item.value, this.props.scale))
                        }
                        color={GraphColors.getLabel(item.color) || item.color}
                        percentage={item.theta}
                    />
                ))}
            </div>
        );
    }
}
