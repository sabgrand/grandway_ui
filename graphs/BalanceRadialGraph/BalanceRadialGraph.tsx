import * as React from 'react';
import _sum from 'lodash-es/sum';
import _round from 'lodash-es/round';
import {bem, connect, fetch} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IFetchHocOutput} from '@steroidsjs/core/hoc/fetch';
import {isPhone} from '@steroidsjs/core/reducers/screen';

import ProfitRadial from '../ProfitRadial';
import DataSelectionPanel from '../DataSelectionPanel';
import {IDataSelectionPanelItems} from '../DataSelectionPanel/DataSelectionPanel';

import PeriodEnum from '../../utilites/enum/PeriodEnum';
import GraphColors from '../../utilites/enum/GraphColors';
import CurrencyEnum from '../../utilites/enum/CurrencyEnum';

import './BalanceRadialGraph.scss';

interface IBalanceRadialGraphProps extends IBemHocOutput, IFetchHocOutput {
    url: string,
    items: {
        attribute: string,
        label: string,
        color: string,
        showDollar?: boolean,
    }[],
    graphData?: any,
    loadManualData?: boolean,
    progressViewItem?: any,
    progressViewItemSize?: 'lg' | 'md' | 'sm',
    progressViewItemType?: 'row' | 'column',
    scale?: number,

    period?: string,
    intervalFrom?: string,
    intervalTo?: string,
    periodsArray?: IDataSelectionPanelItems[],
    onPeriodSelect?: (period, intervalFrom, intervalTo) => void,
    currency?: string,
    hideSelectionPanel?: boolean,
    selectionPanelMobileView?: React.ReactNode,
    hasCardLayout?: boolean,
    graphTotalTemplate?: string,
    className?: CssClassName,
    showFullValueInHint?: boolean,
    radialSize?: number,

    isPhone?: boolean,
}

@fetch(
    (props: IBalanceRadialGraphProps) => {
        if (props.loadManualData) {
            return null;
        }

        if (props.hideSelectionPanel) {
            return {
                url: props.url,
                key: 'graphData',
            };
        }

        const period = props.period || (props.periodsArray && props.periodsArray[0].id);
        const intervalFrom = props.intervalFrom;
        const intervalTo = props.intervalTo;
        return {
            id: `BalanceRadialGraph_${period}_${intervalFrom}_${intervalTo}_${props.url}`,
            url: props.url,
            key: 'graphData',
            params: {
                period,
                intervalFrom,
                intervalTo
            },
        };
    },
    {
        waitLoading: false,
    },
)
@connect(state => ({
    isPhone: isPhone(state)
}))
@bem('BalanceRadialGraph')
export default class BalanceRadialGraph extends React.PureComponent<IBalanceRadialGraphProps> {
    static defaultProps = {
        hideSelectionPanel: false,
        hasCardLayout: true,
    }

    render() {
        const bem = this.props.bem;
        const total:number = (this.props.graphData && !this.props.graphData?.errors)
            ? _sum(this.props.items.map(item => this.props.graphData[item.attribute]))
            : 0;
        const periods = this.props.periodsArray || PeriodEnum.getShortItems();
        const balanceData = this.props.items.map((item, index) => {
            let percent;
            let color;
            let value = null;
            if (total === 0) {
                percent = index % 2 === 0 ? 20 : 30;
                color = index % 2 === 0 ? GraphColors.EMPTY_BLUE_LIGHT : GraphColors.EMPTY_BLUE;
            } else {
                percent = total > 0 ? _round((100 * this.props.graphData[item.attribute]) / total) : 0;
                value = this.props.graphData[item.attribute];
                color = item.color;
            }

            return {
                attribute: item.attribute,
                label: item.label,
                theta: percent,
                value,
                formattedValue: this.props.showFullValueInHint ? `${value}%` : `${percent}%`,
                color,
            };
        });
        const ProgressViewItem = this.props.progressViewItem || null;
        return (
            <div
                className={bem(bem.block({
                    'is-card': this.props.hasCardLayout,
                    centered: !this.props.progressViewItem,
                }),
                this.props.className)}
            >
                {!this.props.hideSelectionPanel && (
                    <DataSelectionPanel
                        className='mb-15'
                        items={periods}
                        selected={periods[0].id}
                        onSelect={(period, intervalFrom = '', intervalTo = '') => {
                            if (this.props.onPeriodSelect) {
                                this.props.onPeriodSelect(period, intervalFrom, intervalTo);
                            } else {
                                this.props.fetchUpdate({period, intervalFrom, intervalTo});
                            }
                        }}
                    />
                )}
                {this.props.isPhone && (this.props.selectionPanelMobileView || null)}
                <div className={bem.element('body')}>
                    {!!ProgressViewItem && (
                        <ProgressViewItem
                            balanceData={balanceData}
                            totalValue={total}
                            currency={this.props.currency}
                            viewType={this.props.progressViewItemType}
                            viewSize={this.props.progressViewItemSize}
                            scale={this.props.scale}
                        />
                    )}
                    <ProfitRadial
                        className={bem.element('radial')}
                        graphArray={balanceData.map(item => ({
                            attribute: item.attribute,
                            theta: item.theta,
                            label: item.formattedValue,
                            legendLabel: item.label,
                            value: item.value,
                            color: item.color,
                        }))}
                        currency={this.props.currency}
                        totalSum={this.getLabelForTotalSum(total)}
                        isEmpty={total === 0}
                        showLegend={!ProgressViewItem}
                        radialSize={this.props.radialSize}
                        scale={this.props.scale}
                    />
                </div>
            </div>
        );
    }

    getLabelForTotalSum(total: number) {
        if (this.props.graphTotalTemplate) {
            return this.props.graphTotalTemplate.replace('total', _round(total, 2));
        }

        if (this.props.currency === CurrencyEnum.RUB) {
            return CurrencyEnum.format(total, this.props.currency, true, this.props.scale)
        }

        return this.props.currency === CurrencyEnum.USD
            ? CurrencyEnum.format(total, this.props.currency, true, this.props.scale)
            : CurrencyEnum.format(total, '', true, this.props.scale)
    }
}
