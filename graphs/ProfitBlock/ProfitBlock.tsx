import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import './ProfitBlock.scss';

export interface IProfitBlockDataItem {
    label: string,
    value: number | string,
    additionalValue?: number | string,
    valueGrowth?: number,
}

interface IProfitBlockProps extends IBemHocOutput {
    title?: string,
    data: IProfitBlockDataItem[],
    className?: CssClassName,
}

@bem('ProfitBlock')
export default class ProfitBlock extends React.PureComponent<IProfitBlockProps> {
    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                {this.props.title && (
                    <div className={bem.element('title')}>
                        {this.props.title}
                    </div>
                )}
                <div className={bem.element('body')}>
                    {this.props.data.map((item, index) => (
                        <div key={index} className={bem.element('item')}>
                            <div className={bem.element('label')}>
                                {item.label}
                            </div>
                            <div className={bem.element('value')}>
                                {item.value || 0}
                                {item.valueGrowth && (
                                    <span
                                        className={bem.element('value-growth', {
                                            'positive': item.valueGrowth > 0,
                                            'negative': item.valueGrowth < 0,
                                        })}
                                    >
                                    {`${item.valueGrowth}%`}
                                </span>
                                )}
                            </div>
                            {item.additionalValue && (
                                <div
                                    className={bem.element('value')}
                                    key={2}
                                >
                                    {item.additionalValue || 0}
                                </div>
                            )}
                        </div>
                    ))}
                </div>
            </div>
        );
    }
}
