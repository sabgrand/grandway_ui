import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import moment from 'moment';

import './DataSelectionPanel.scss';
import PeriodEnum from '../../utilites/enum/PeriodEnum';


export interface IDataSelectionPanelItems {
    id: string,
    label: string,
}

interface IDataSelectionPanelProps extends IBemHocOutput {
    items: IDataSelectionPanelItems[],
    selected?: string,
    onSelect: (id: string, intervalFrom?: string, intervalTo?: string) => void,
    className?: CssClassName,
}

interface IDataSelectionPanelState {
    selectedId: string,
    selectedLabel: string,
    intervalFrom: string,
    intervalTo: string,
}

@bem('DataSelectionPanel')
export default class DataSelectionPanel extends React.PureComponent<IDataSelectionPanelProps, IDataSelectionPanelState> {

    constructor(props) {
        super(props);

        this.state = {
            selectedId: this.props.selected || this.props.items[0].id,
            selectedLabel: this.props.selected || this.props.items[0].label,
            intervalFrom: moment().subtract(1, 'months').format('YYYY-MM-DD'),
            intervalTo: moment().format('YYYY-MM-DD'),
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.selected !== this.props.selected) {
            this.setState({selectedId: this.props.selected});
            this.setState({intervalFrom: this.state.intervalFrom});
            this.setState({intervalTo: this.state.intervalTo});
        }
    }

    render() {
        const bem = this.props.bem;
        const selectedIndex = this.props.items.findIndex(item => item.id === this.state.selectedId);
        return (
            <div className={bem(bem.block(), this.props.className)}>
                <div className={bem.element('switch')}>
                    {this.props.items.map((item, index) => (
                        <div
                            key={item.id}
                            className={bem.element('switch__item', {
                                'active': item.id === this.state.selectedId,
                                'border-left': index !== 0 && selectedIndex > index,
                                'border-right': index !== (this.props.items.length - 1) && selectedIndex < index
                            })}
                            onClick={() => {
                                this.setState({selectedId: item.id});
                                this.setState({selectedLabel: item.label});
                                this.props.onSelect(item.id,
                                    this.state.intervalFrom,
                                    this.state.intervalTo
                                );
                            }}
                        >
                            {item.label}
                        </div>
                    ))}
                </div>
                <div className={bem.element('interval', {
                        'hidden': this.state.selectedId !== PeriodEnum.INTERVAL,
                    })}>
                    <span>
                        <label>С</label>
                        <input
                            key='intervalFrom'
                            name='intervalFrom'
                            type='date'
                            className={bem.element('interval_input')}
                            value={this.state.intervalFrom}
                            onChange={e => {
                                this.setState({intervalFrom: e.target.value});
                                this.props.onSelect(
                                    this.state.selectedId,
                                    e.target.value,
                                    this.state.intervalTo
                                );
                            }}
                            />
                    </span>
                    <span>
                        <label>По</label>
                        <input
                            key='intervalTo'
                            name='intervalTo'
                            type='date'
                            className={bem.element('interval_input')}
                            value={this.state.intervalTo}
                            onChange={e => {
                                this.setState({intervalTo: e.target.value});
                                this.props.onSelect(
                                    this.state.selectedId,
                                    this.state.intervalFrom,
                                    e.target.value
                                );
                            }}
                            />
                    </span>
                </div>
            </div>
        );
    }
}