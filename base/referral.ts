const KEY_PREFIX = 'gwref_';
const EXPIRES_DAYS = 60;

export interface IReferralData {
    invite: string,
    referral: string,
    product: string,
    tariff: string,
}

const attributes = [
    'invite',
    'referral',
    'product',
    'tariff',
];

// Get referral/invite data
export const referralGet = (clientStorage) : IReferralData => {
    const data: IReferralData = {
        invite: null,
        referral: null,
        product: null,
        tariff: null,
    };
    attributes.forEach(attribute => {
        data[attribute] = clientStorage.get(KEY_PREFIX + attribute, clientStorage.STORAGE_COOKIE) || null;
    });
    return data;
}

// Set referral/invite data (from loaded invite)
export const referralSet = (clientStorage, data: IReferralData) => {
    attributes.forEach(attribute => {
        if (data[attribute]) {
            clientStorage.set(KEY_PREFIX + attribute, data[attribute], clientStorage.STORAGE_COOKIE, EXPIRES_DAYS);
        } else {
            clientStorage.remove(KEY_PREFIX + attribute, clientStorage.STORAGE_COOKIE);
        }
    });
}

// Store referral/invite data to cookie
export const referralInit = (clientStorage) => {
    // Load from url
    const urlSearchParams = new URLSearchParams(location.search);
    const data: IReferralData = {
        invite: null,
        referral: null,
        product: null,
        tariff: null,
    };
    attributes.forEach(attribute => {
        data[attribute] = urlSearchParams.get(attribute) || null;
    });

    if (data.invite || data.referral) {
        referralSet(clientStorage, data);
    }
}