import CrudView from '@steroidsjs/bootstrap/crud/Crud/CrudView';
// import AutoCompleteFieldView from '@steroidsjs/bootstrap/form/AutoCompleteField/AutoCompleteFieldView';
// import ButtonView from '@steroidsjs/bootstrap/form/Button/ButtonView';
// import CheckboxFieldView from '@steroidsjs/bootstrap/form/CheckboxField/CheckboxFieldView';
import CheckboxListFieldView from '@steroidsjs/bootstrap/form/CheckboxListField/CheckboxListFieldView';
// import DateFieldView from '@steroidsjs/bootstrap/form/DateField/DateFieldView';
import DateTimeFieldView from '@steroidsjs/bootstrap/form/DateTimeField/DateTimeFieldView';
// import DropDownFieldView from '@steroidsjs/bootstrap/form/DropDownField/DropDownFieldView';
import FieldSetView from '@steroidsjs/bootstrap/form/FieldSet/FieldSetView';
// import FieldLayoutView from '@steroidsjs/bootstrap/form/FieldLayout/FieldLayoutView';
import FieldListView from '@steroidsjs/bootstrap/form/FieldList/FieldListView';
import FieldListItemView from '@steroidsjs/bootstrap/form/FieldList/FieldListItemView';
// import FileFieldView from '@steroidsjs/bootstrap/form/FileField/FileFieldView';
// import FileFieldItemView from '@steroidsjs/bootstrap/form/FileField/FileFieldItemView';
import FormView from '@steroidsjs/bootstrap/form/Form/FormView';
//import HtmlFieldView from '@steroidsjs/bootstrap/form/HtmlField/HtmlFieldView';
// import InputFieldView from '@steroidsjs/bootstrap/form/InputField/InputFieldView';
import NumberFieldView from '@steroidsjs/bootstrap/form/NumberField/NumberFieldView';
// import PasswordFieldView from '@steroidsjs/bootstrap/form/PasswordField/PasswordFieldView';
// import RadioListFieldView from '@steroidsjs/bootstrap/form/RadioListField/RadioListFieldView';
import RangeFieldView from '@steroidsjs/bootstrap/form/RangeField/RangeFieldView';
import ReCaptchaFieldView from '@steroidsjs/bootstrap/form/ReCaptchaField/ReCaptchaFieldView';
import SliderFieldView from '@steroidsjs/bootstrap/form/SliderField/SliderFieldView';
import SwitcherFieldView from '@steroidsjs/bootstrap/form/SwitcherField/SwitcherFieldView';
import TextFieldView from '@steroidsjs/bootstrap/form/TextField/TextFieldView';
import TimeFieldView from '@steroidsjs/bootstrap/form/TimeField/TimeFieldView';
import BooleanFormatterView from '@steroidsjs/bootstrap/format/BooleanFormatter/BooleanFormatterView';
import DefaultFormatterView from '@steroidsjs/bootstrap/format/DefaultFormatter/DefaultFormatterView';
import PhotosFormatterView from '@steroidsjs/bootstrap/format/PhotosFormatter/PhotosFormatterView';
// import IconView from '@steroidsjs/bootstrap/icon/Icon/IconView';
// import HeaderView from '@steroidsjs/bootstrap/layout/Header/HeaderView';
import LoaderView from '@steroidsjs/bootstrap/layout/Loader/LoaderView';
// import NotificationsItemView from '@steroidsjs/bootstrap/layout/Notifications/NotificationsItemView';
import NotificationsView from '@steroidsjs/bootstrap/layout/Notifications/NotificationsView';
// import TooltipView from '@steroidsjs/bootstrap/layout/Tooltip/TooltipView';
import AccordionView from '@steroidsjs/bootstrap/list/Accordion/AccordionView';
import ControlsColumnView from '@steroidsjs/bootstrap/list/ControlsColumnView/ControlsColumnView';
import CheckboxColumnView from '@steroidsjs/bootstrap/list/CheckboxColumn/CheckboxColumnView';
import DetailView from '@steroidsjs/bootstrap/list/Detail/DetailView';
import EmptyView from '@steroidsjs/bootstrap/list/Empty/EmptyView';
// import GridView from '@steroidsjs/bootstrap/list/Grid/GridView';
// import ListView from '@steroidsjs/bootstrap/list/List/ListView';
import ListItemView from '@steroidsjs/bootstrap/list/List/ListItemView';
import PaginationMoreView from '@steroidsjs/bootstrap/list/Pagination/PaginationMoreView';
// import PaginationButtonView from '@steroidsjs/bootstrap/list/Pagination/PaginationButtonView';
import PaginationSizeView from '@steroidsjs/bootstrap/list/PaginationSize/PaginationSizeView';
// import ModalView from '@steroidsjs/bootstrap/modal/Modal/ModalView';
import TwoFactorModalView from '@steroidsjs/bootstrap/modal/TwoFactorModal/TwoFactorModalView';
import BreadcrumbsView from '@steroidsjs/bootstrap/nav/Breadcrubms/BreadcrumbsView';
import ControlsView from '@steroidsjs/bootstrap/nav/Controls/ControlsView';
import NavBarView from '@steroidsjs/bootstrap/nav/Nav/NavBarView';
import NavButtonView from '@steroidsjs/bootstrap/nav/Nav/NavButtonView';
import NavIconView from '@steroidsjs/bootstrap/nav/Nav/NavIconView';
import NavLinkView from '@steroidsjs/bootstrap/nav/Nav/NavLinkView';
import NavListView from '@steroidsjs/bootstrap/nav/Nav/NavListView';
// import NavTabsView from '@steroidsjs/bootstrap/nav/Nav/NavTabsView';
import TreeView from '@steroidsjs/bootstrap/nav/Tree/TreeView';

export default {
    'crud.CrudView': CrudView,
    //'form.AutoCompleteFieldView': AutoCompleteFieldView,
    //'form.ButtonView': ButtonView,
    //'form.CheckboxFieldView': CheckboxFieldView,
    'form.CheckboxListFieldView': CheckboxListFieldView,
    //'form.DateFieldView': DateFieldView,
    'form.DateTimeFieldView': DateTimeFieldView,
    //'form.DropDownFieldView': DropDownFieldView,
    //'form.FieldLayoutView': FieldLayoutView,
    'form.FieldSetView': FieldSetView,
    'form.FieldListView': FieldListView,
    'form.FieldListItemView': FieldListItemView,
    //'form.FileFieldView': FileFieldView,
    //'form.FileFieldItemView': FileFieldItemView,
    'form.FormView': FormView,
    //'form.HtmlFieldView': HtmlFieldView,
    //'form.InputFieldView': InputFieldView,
    'form.NumberFieldView': NumberFieldView,
    //'form.PasswordFieldView': PasswordFieldView,
    //'form.RadioListFieldView': RadioListFieldView,
    'form.RangeFieldView': RangeFieldView,
    'form.ReCaptchaFieldView': ReCaptchaFieldView,
    'form.SliderFieldView': SliderFieldView,
    'form.SwitcherFieldView': SwitcherFieldView,
    'form.TextFieldView': TextFieldView,
    'form.TimeFieldView': TimeFieldView,
    'format.BooleanFormatterView': BooleanFormatterView,
    'format.DefaultFormatterView': DefaultFormatterView,
    'format.PhotosFormatterView': PhotosFormatterView,
    //'icon.IconView': IconView,
    //'layout.HeaderView': HeaderView,
    'layout.LoaderView': LoaderView,
    'layout.NotificationsView': NotificationsView,
    //'layout.NotificationsItemView': NotificationsItemView,
    //'layout.TooltipView': TooltipView,
    'list.AccordionView': AccordionView,
    'list.ControlsColumnView': ControlsColumnView,
    'list.CheckboxColumnView': CheckboxColumnView,
    'list.DetailView': DetailView,
    'list.EmptyView': EmptyView,
    //'list.GridView': GridView,
    //'list.ListView': ListView,
    'list.ListItemView': ListItemView,
    //'list.PaginationButtonView': PaginationButtonView,
    'list.PaginationMoreView': PaginationMoreView,
    'list.PaginationSizeView': PaginationSizeView,
    //'modal.ModalView': ModalView,
    'modal.TwoFactorModalView': TwoFactorModalView,
    'nav.BreadcrumbsView': BreadcrumbsView,
    'nav.ControlsView': ControlsView,
    'nav.NavBarView': NavBarView,
    'nav.NavButtonView': NavButtonView,
    'nav.NavIconView': NavIconView,
    'nav.NavLinkView': NavLinkView,
    'nav.NavListView': NavListView,
    //'nav.NavTabsView': NavTabsView,
    'nav.TreeView': TreeView,
};
