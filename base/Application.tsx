import * as React from 'react';
import {application, components} from '@steroidsjs/core/hoc';

import 'style/index.scss';
import ResourceComponent from '@steroidsjs/core/components/ResourceComponent';
import {IComponentsHocOutput} from '@steroidsjs/core/hoc/components';
import {referralInit} from './referral';
import HttpComponent from '../components/HttpComponent';

interface IApplication extends IComponentsHocOutput {
    onInit?: any,
}

@application({
    components: {
        http: {
            className: HttpComponent,
            apiUrl: typeof location === 'undefined' || !location.port ? process.env.APP_BACKEND_URL : null,
        },
        resource: {
            className: ResourceComponent,
            googleCaptchaSiteKey: process.env.APP_GOOGLE_CAPTCHA_SITE_KEY,
            googleApiKey: process.env.APP_GOOGLE_MAP_API_KEY,
        },
    },
    onInit: ({ui}) => {
        ui.addViews(require('./bootstrap').default);
        ui.addViews(require.context('..', true, /(?!node_modules)\/.+View\.(j|t)sx?$/));
        ui.addFields(require('./fields').default);
        ui.addFormatters(require('@steroidsjs/core/ui/format').default);
        ui.addIcons({
            ...require('../icons').default,
        });
    },
})
@components()
export default class Application extends React.PureComponent<IApplication> {
    componentDidMount() {
        // Store referral/invite data to cookie
        referralInit(this.props.components.clientStorage);

        if (this.props.onInit) {
            this.props.onInit(this.props.components);
        }
    }

    render() {
        return this.props.children;
    }
}
