import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Icon from '@steroidsjs/core/ui/icon/Icon';
import Accordeon from '../Accordeon';

interface IQuestions {
    question: string,
    answer: string | JSX.Element
}

interface IFAQSectionState {
    activeTopic: number,
    questions: IQuestions[]
}

interface IFAQSectionProps extends IBemHocOutput {
    settings: {
        topic: string,
        questions: IQuestions[]
    }[],
    modificator?: 'orange' | 'green'
}


import './FAQSection.scss';

@bem('FAQSection')
export default class FAQSection extends React.PureComponent<IFAQSectionProps, IFAQSectionState> {

    constructor(props) {
        super(props);

        this.state = {
            activeTopic: 0,
            questions: this.props.settings[0].questions
        };
    }

    render() {
        const bem = this.props.bem;

        return (
            <div className={bem.block({[this.props.modificator]: !!this.props.modificator})}>
                <div className={bem.element('questions')}>
                    <ul className={bem.element('questions-list')}>
                        {this.props.settings.map((question, index) => (
                            <li
                                className={bem.element('question', {'active': this.state.activeTopic === index})}
                                onClick={() => {
                                    this.setState({
                                        activeTopic: index,
                                        questions: question.questions
                                    });
                                }}
                                key={index}
                            >
                                <Icon name='whiteStar' className='mr-10'/>
                                {question.topic}
                            </li>
                        ))}
                    </ul>
                </div>

                <div className={bem.element('answers')}>
                    <Accordeon items={this.state.questions} modificator={this.props.modificator}/>
                </div>
            </div>
        );
    }
}