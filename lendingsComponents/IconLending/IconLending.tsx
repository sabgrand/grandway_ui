import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';

import Icon, {IIconProps} from '@steroidsjs/core/ui/icon/Icon/Icon';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

interface IIconLendingProps extends IBemHocOutput, IIconProps {
    size?: {
        width?: number | string,
        height?: number | string
    },
    name: string
}

@bem('IconLending')
export default class IconLending extends React.PureComponent<IIconLendingProps> {

    static defaultProps = {
        size: {
            width: 'auto',
            height: 'auto'
        }
    }

    render() {
        const bem = this.props.bem;

        return (
            <Icon style={this.props.size} {...this.props} />
        );
    }
}