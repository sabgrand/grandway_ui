import * as React from 'react';
import {HashLink} from 'react-router-hash-link';
import {bem, connect} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IConnectHocOutput} from '@steroidsjs/core/hoc/connect';
import {getActiveRouteIds} from '@steroidsjs/core/reducers/router';

import Link from '@steroidsjs/core/ui/nav/Link/index';
import MediasMap from '../Footer/views/MediasMap';
import IconLending from '../IconLending';
import {ISidebarData} from '../Header/Header';
import AuthRegistrationLinks from './views/AuthRegistrationLinks';

import './Sidebar.scss';

interface ISibebarProps extends IBemHocOutput, IConnectHocOutput {
    onItemClick: (number) => void,
    onClose: () => void,
    activeRouteIds?: any,
    activeMenuItem: number,
    icon: string,
    sidebarData: Array<ISidebarData>,
    routeRoot: string,
    modificator: string
}

@connect(state => ({
    activeRouteIds: getActiveRouteIds(state)[0],
}))
@bem('Sidebar')
export default class Sidebar extends React.PureComponent<ISibebarProps> {

    constructor(props) {
        super(props);

        this.state = {
            componentIsMounted: false
        };
    }

    componentDidMount() {
        document.body.style.overflow = 'hidden';
    }

    componentWillUnmount() {
        document.body.style.overflow = 'auto';
    }

    render() {
        const bem = this.props.bem;

        return (
            <aside className={bem.block({[this.props.modificator]: true})}>
                <nav className={bem.element('nav')}>
                    <div className={bem.element('logo-container')}>
                        <Link
                            toRoute={this.props.routeRoot}
                            onClick={this.props.onClose}
                        >
                            <IconLending name={this.props.icon}/>
                        </Link>
                    </div>
                    <div
                        className={bem.element('close-button')}
                        onClick={this.props.onClose}
                    />
                    <div className={bem.element('routes-container')}>
                        <div className={bem.element('auth-wrapper')}>
                            <AuthRegistrationLinks />
                        </div>
                        <ul className={bem.element('list')}>
                            {this.renderSidebarData()}
                        </ul>
                    </div>
                    <div className={bem.element('footer')}>
                        <img
                            className={bem.element('logo')}
                            src='/images/footer-logo.png'
                            alt='card-logo'
                        />
                        <div className={bem.element('medias')}>
                            <MediasMap/>
                        </div>
                    </div>
                </nav>
            </aside>
        );
    }

    renderSidebarData() {
        const bem = this.props.bem;

        return (
            this.props.sidebarData.map((dataItem, index) =>
                this.getSidebarItem(dataItem, index)
            )
        );
    }

    getSidebarItem(dataItem: ISidebarData, index: number) {
        const bem = this.props.bem;

        const HashLinkWrapper: React.FC<{ anchorLink: string | null, index: number }> = (props) => {
            if (props.anchorLink === null) {
                return (
                    <>
                        {props.children}
                    </>
                );
            }

            return (
                <HashLink
                    className={bem.element('navItem',
                        {'active': this.props.activeMenuItem === index})
                    }
                    to={props.anchorLink}
                    onClick={this.props.onClose}
                >
                    {props.children}
                </HashLink>
            );
        };

        return (
            <HashLinkWrapper
                anchorLink={dataItem.anchor}
                index={index}
                key={index}
            >
                <li
                    className={bem.element('navItem',
                        {'active': this.props.activeMenuItem === index})
                    }
                    onClick={() => this.props.onItemClick(index)}
                >
                    {dataItem.label}
                    {(dataItem.items && this.props.activeMenuItem === index) && (
                        <ul className={bem.element('subNav')}>
                            {dataItem.items.map((subNavItem, index) =>
                                <li
                                    key={index}
                                >
                                    <Link
                                        className={bem.element('subitem',
                                            {'active': subNavItem.route === this.props.activeRouteIds})
                                        }
                                        toRoute={subNavItem.route}
                                        onClick={this.props.onClose}
                                    >
                                        {subNavItem.label}
                                    </Link>
                                </li>
                            )}
                        </ul>
                    )}
                </li>
            </HashLinkWrapper>
        );
    }
}
