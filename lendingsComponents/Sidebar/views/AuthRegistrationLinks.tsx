import * as React from 'react';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import Link from '@steroidsjs/core/ui/nav/Link';
import {bem} from '@steroidsjs/core/hoc';
import {urlAccountLogin, urlAccountRegistration} from '../../../config/constants';

import './AuthRegistrationLinks.scss';

interface IAuthRegistrationLinksProps extends IBemHocOutput {
}

@bem('AuthRegistrationLinks')
export default class AuthRegistrationLinks extends React.PureComponent<IAuthRegistrationLinksProps> {
    render() {
        const bem = this.props.bem;
        return (
            <>
                <Link
                    className={bem.element('route')}
                    target='_blank'
                    url={urlAccountLogin}
                    href={urlAccountLogin}
                    label={__('Вход')}
                />
                <Link
                    className={bem.element('route')}
                    target='_blank'
                    url={urlAccountRegistration}
                    href={urlAccountRegistration}
                    label={__('Регистрация')}
                />
            </>
        );
    }
}
