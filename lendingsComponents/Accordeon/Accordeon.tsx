import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import Icon from '@steroidsjs/core/ui/icon/Icon';

import './Accordeon.scss';

interface IAccordeonItem {
    question: string,
    answer: string | JSX.Element
}

interface IAcordeonProps extends IBemHocOutput {
    items: Array<IAccordeonItem>,
    modificator?: 'orange' | 'green'
}

interface IAccordeonState {
    indexOpenedElement: number
}

@bem('Accordeon')
export default class Accordeon extends React.PureComponent<IAcordeonProps, IAccordeonState> {

    constructor(props) {
        super(props);

        this.state = {
            indexOpenedElement: null
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps: Readonly<IAcordeonProps>, nextContext: any) {
        this.setState({indexOpenedElement: null});
    }

    render() {
        const bem = this.props.bem;

        return (
            this.props.items.map((item, index) => (
                <div className={
                    bem.block({[this.props.modificator]: !!this.props.modificator})}
                     key={index}
                >
                    <div className={bem.element('body')}>
                        <div
                            className={bem.element('header')}
                            onClick={() => {
                                if (this.state.indexOpenedElement === index) {
                                    this.setState({indexOpenedElement: null});
                                } else {
                                    this.setState({indexOpenedElement: index});
                                }
                            }}
                        >
                            <div className={bem.element('icon-container')}>
                                <Icon className={bem.element('icon', {
                                    'minus': this.state.indexOpenedElement === index,
                                    'plus': this.state.indexOpenedElement !== index
                                })}
                                      name={this.state.indexOpenedElement === index && 'minus' || 'plus'}/>
                            </div>
                            <p className={bem.element('title')}>
                                {item.question}
                            </p>
                        </div>
                        <div className={bem.element('collapse', {'open': this.state.indexOpenedElement === index})}>
                            {typeof (item.answer) === 'string'
                                ? (<p className={bem.element('description')}>
                                        {item.answer}
                                    </p>
                                ) : (
                                    item.answer
                                )}
                        </div>
                    </div>
                </div>
            ))
        );
    }
}
