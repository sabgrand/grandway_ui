import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {IConnectHocOutput} from '@steroidsjs/core/hoc/connect';
import {connect, screen} from '@steroidsjs/core/hoc/index';
import {isPhone} from '@steroidsjs/core/reducers/screen';
import {SCREEN_CONFIG} from '../../config/constants';

import SteroidHeader from '@steroidsjs/core/ui/layout/Header';
import Link from '@steroidsjs/core/ui/nav/Link';
import IconLending from '../IconLending';
import Sidebar from '../Sidebar';

interface IHeaderState {
    activeTab: number,
    isShowSidebar: boolean
}

interface IHeaderNavItems {
    id: number,
    label: string,
    dropdown?: Array<{
        route: string,
        title: string
    }>,
    toRoute?: string
}

export interface ISidebarData {
    label: string,
    items: Array<{
        route: string,
        label: string
    }> | null,
    anchor?: string
}

interface IHeaderProps extends IBemHocOutput, IConnectHocOutput {
    isPhone?: boolean,
    navItems?: Array<IHeaderNavItems>,
    sidebarData: Array<ISidebarData>,
    routeRoot: string,
    mainColor?: string,
    telephoneNumber: string,
    serviceName: string
}

import './Header.scss';


@screen(SCREEN_CONFIG)
@connect(state => ({
    isPhone: isPhone(state)
}))
@bem('Header')
export default class Header extends React.PureComponent<IHeaderProps, IHeaderState> {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 10,
            isShowSidebar: false
        };
    }

    _closeSidebar = () => this.setState({isShowSidebar: false});

    _setActiveTav = (numberActiveTab) => this.setState({activeTab: numberActiveTab})

    render() {
        return this.props.isPhone ? this.mobileHeader() : this.desktopHeader();
    }

    mobileHeader() {
        const bem = this.props.bem;

        const IconView = (iconName) => (props) => {

            if (iconName === 'phone') {
                return (
                    <a href={`tel:${this.props.telephoneNumber}`}>
                        <IconLending name={iconName}/>
                    </a>
                );
            }

            return (
                <>
                    <IconLending
                        name={iconName}
                        onClick={() => this.setState({isShowSidebar: true})}
                    />
                    {this.state.isShowSidebar && (
                        <Sidebar
                            activeMenuItem={this.state.activeTab === 10 ? 0 : this.state.activeTab}
                            onClose={this._closeSidebar}
                            onItemClick={(numberActiveTab) => this._setActiveTav(numberActiveTab)}
                            icon={this.props.serviceName}
                            sidebarData={this.props.sidebarData}
                            routeRoot={this.props.routeRoot}
                            modificator={this.props.serviceName}
                        />
                    )}
                </>
            );
        };

        return (
            <>
                <SteroidHeader
                    logo={{
                        title: __('HomeWay'),
                        linkProps: {onClick: () => this.setState({activeTab: 10})},
                    }}

                    className={bem.block()}
                    nav={{
                        onChange: (id) => this.setState({activeTab: id}),
                        activeTab: this.state.activeTab,
                        items: [
                            {
                                id: 0,
                                label: __('Звонок'),
                                view: IconView('phone')
                            },
                            {
                                id: 1,
                                label: __('Меню'),
                                view: IconView('burger')
                            }
                        ]
                    }}
                />
            </>
        );
    }

    desktopHeader() {
        const bem = this.props.bem;

        const Dropdown = (dropdownLinks) => (props) => {
            return (
                <div className={bem.element('dropdown')}>
                    <props.tag className={props.className}>
                        {props.label}
                    </props.tag>
                    <div className={bem.element('collapse', {[this.props.mainColor]: !!this.props.mainColor})}>
                        {dropdownLinks.map((link, item) =>
                            <Link
                                onClick={props.onClick}
                                toRoute={link.route}
                                key={item}
                            >
                                {link.title}
                            </Link>
                        )}
                    </div>
                </div>
            );
        };

        return (
            <SteroidHeader
                logo={{
                    title: __('HomeWay'),
                    linkProps: {onClick: () => this.setState({activeTab: 10})},
                }}

                nav={{
                    onChange: (id) => this.setState({activeTab: id}),
                    activeTab: this.state.activeTab,
                    items: this.props.navItems.map(navItem => ({
                        ...navItem,
                        view: navItem.dropdown ? Dropdown(navItem.dropdown) : null
                    }))
                }}
                className={bem.block()}
            >
                {this.props.children}
            </SteroidHeader>
        );
    }
}

