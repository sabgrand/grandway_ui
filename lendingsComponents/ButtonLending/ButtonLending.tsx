import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';

import Button, {IButtonProps} from '@steroidsjs/core/ui/form/Button/Button';

import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import './ButtonLending.scss';

interface IButtonLendingProps extends IBemHocOutput, IButtonProps {
    primary?: boolean,
    secondary?: boolean,
    containerSize?: {
        width?: number,
        height?: number
    }
}

@bem('ButtonLending')
export default class ButtonLending extends React.PureComponent<IButtonLendingProps> {

    static defaultProps = {
        containerSize: {
            width: 249,
            height: 53
        }
    }

    render() {
        const bem = this.props.bem;

        return (
            <Button
                {...this.props}
                className={bem(
                    bem.block({
                        'primary': this.props.primary,
                        'secondary': this.props.secondary
                    }),
                    this.props.className
                )}
                style={{...this.props.containerSize}}
            >
                {this.props.children}
            </Button>
        );
    }
}
