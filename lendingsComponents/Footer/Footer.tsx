import * as React from 'react';
import {HashLink} from 'react-router-hash-link';
import {bem, connect} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {isPhone} from '@steroidsjs/core/reducers/screen';
import {IConnectHocOutput} from '@steroidsjs/core/hoc/connect';
import {getActiveRouteIds} from '@steroidsjs/core/reducers/router';

import Link from '@steroidsjs/core/ui/nav/Link';
import MediasMap from './views/MediasMap';

import './Footer.scss';

interface IFooterProps extends IBemHocOutput, IConnectHocOutput {
    isSecondColor?: boolean,
    isPhone?: boolean,
    navigation: any,
    agreements: any,
    secondColor: 'orange' | 'green',
}

@connect(state => ({
    isSecondColor: getActiveRouteIds(state).length >= 2,
    isPhone: isPhone(state)
}))
@bem('Footer')
export default class Footer extends React.PureComponent<IFooterProps> {

    render() {
        const bem = this.props.bem;
        const navigation = this.props.navigation;
        const agreements = this.props.agreements;
        const isSecondColor = this.props.isSecondColor || this.props.isPhone;

        const buildDatetime = process.env.APP_BUILD_DATETIME || '';
        const buildCode = process.env.APP_BUILD_CODE || '';

        return (
            <footer className={
                bem.block({
                    'isSecondColor': isSecondColor,
                    [this.props.secondColor]: isSecondColor})}>
                <div className={bem.element('container')}>
                    <div className={bem.element('main')}>
                        <div className={bem.element('products')}>
                            <div className={bem.element('logo-wrapper')}>
                                {this.props.children}
                            </div>
                            <div className={bem.element('media')}>
                                <MediasMap />
                            </div>
                        </div>
                        <div className={bem.element('navigation')}>
                            {
                                navigation.map((navigationItem, index) => (
                                    <div className={bem.element('navigation-column')} key={index}>
                                        <p className={bem.element('navigation-title')}>
                                            {navigationItem.title}
                                        </p>
                                        <ul className={bem.element('navigation-list')}>
                                            {navigationItem.items.map((link, index) => (
                                                <li key={index}>
                                                    {link.anchorLink && (
                                                        <HashLink
                                                            className={bem.element('link')}
                                                            smooth
                                                            to={link.to}
                                                        >
                                                            {link.name}
                                                        </HashLink>
                                                    ) || (
                                                        <Link
                                                            className={bem.element('link')}
                                                            toRoute={link.to}
                                                        >
                                                            {link.name}
                                                        </Link>
                                                    )}
                                                </li>
                                            ))}
                                        </ul>
                                    </div>
                                ))
                            }
                        </div>
                    </div>
                    <div className={bem.element('agreement')}>
                        {agreements.map((agreement, index) => (
                            <Link
                                className={bem.element('link')}
                                url={agreement.url}
                                target='_blank'
                                key={index}
                            >
                                {agreement.name}
                            </Link>
                        ))}
                    </div>
                    <div className={bem.element('version-data-container')}>
                        {buildDatetime && (
                            <p className={bem.element('version-data')}>
                                {buildDatetime}
                            </p>
                        )}
                        {buildCode && (
                            <p className={bem.element('version-data')}>
                                {buildCode}
                            </p>
                        )}
                    </div>
                </div>
            </footer>
        );
    }
}
