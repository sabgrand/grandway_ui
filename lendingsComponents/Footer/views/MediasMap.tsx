import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import medias from '../../data/media';

interface IMediasMapProps extends IBemHocOutput {
}

@bem('MediasMap')
export default class MediasMap extends React.PureComponent<IMediasMapProps> {
    render() {
        return (
            medias.map((media, index) => (
                <a
                    key={index}
                    href={media.href}
                >
                    <img src={media.src} alt={media.alt}/>
                </a>
            ))
        );
    }
}
