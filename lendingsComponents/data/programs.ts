export default [
    {
        src: '/images/programs/grandway.svg',
        alt: 'grandway',
        href: 'https://grandway.world'
    },
    {
        src: '/images/programs/profitway.svg',
        alt: 'profitway',
        href: 'https://profitway.world'
    },
    {
        src: '/images/programs/homeway.svg',
        alt: 'homeway',
        href: 'https://homeway.world/'
    },
    {
        src: '/images/programs/storeway.svg',
        alt: 'storeway',
        href: '#'
    },
];