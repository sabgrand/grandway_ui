export default [
    // {
    //     src: '/images/media/instagram-grandway.svg',
    //     alt: 'instagram-grandway',
    //     href: 'https://www.instagram.com/grandway_official/',
    // },
    {
        src: '/images/media/vk.svg',
        alt: 'vk',
        href: 'https://vk.com/grandway_holding',
    },
    {
        src: '/images/media/youtube.svg',
        alt: 'youtube',
        href: 'https://www.youtube.com/channel/UCz51bDfphaG4W4Buozf-IrQ',
    },
    // {
    //     src: '/images/media/instagram-homeway.svg',
    //     alt: 'instagram-homeway',
    //     href: 'https://www.instagram.com/grandway_official/',
    // },
    // {
    //     src: '/images/media/twitter.svg',
    //     alt: 'twitter',
    //     href: 'https://twitter.com/grand_way',
    // },
    // {
    //     src: '/images/media/facebook.svg',
    //     alt: 'facebook',
    //     href: 'https://www.facebook.com/YOURGrandWay',
    // },
]
