import * as React from 'react';
import {bem, components} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

import Icon from '@steroidsjs/core/ui/icon/Icon';
import {Link} from '@steroidsjs/core/ui/nav';

import {IArticle} from '../../types/IArticle';

import './NewsCard.scss';

export interface INewsCardProps extends IBemHocOutput {
    item: IArticle,
    buttonProps?: (item: IArticle) => any,
    className?: string,
}

@components('locale')
@bem('NewsCard')
export default class NewsCard extends React.PureComponent<INewsCardProps> {
    render() {
        const bem = this.props.bem;
        const item = this.props.item;
        return (
            <div
                className={bem(bem.block(), this.props.className)}>
                <div className={bem.element('preview-container')}>
                    <img
                        className={bem.element('preview')}
                        src={item.imageThumbnailUrl || '/images/utility/default-preview.png'}
                        alt={item.title}
                    />
                </div>
                <div className={bem.element('body')}>
                    <div className={bem.element('type-container')}>
                        <span className={bem.element('data')}>
                            {this.props.locale.moment(item.createTime).format('DD.MM.YYYY')}
                        </span>
                        <span className={bem.element('type')}>
                            {item.category?.title || null}
                        </span>
                    </div>
                    <h6 className={bem.element('title')}>
                        {item.title}
                    </h6>
                    <p className={bem.element('description')}>
                        {item.description}
                    </p>
                    {this.props.buttonProps && (
                        <div className={bem.element('link')}>
                            <Link
                                {...this.props.buttonProps(item)}
                                linkAsButton
                            >
                                {__('Читать полностью').toUpperCase()}
                                <div className={bem.element('icon-wrapper')}>
                                    <Icon
                                        className={bem.element('link-icon')}
                                        name='chevron_right'
                                    />
                                </div>
                            </Link>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
