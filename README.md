# GrandWay UI Kit

Набор программных компонентов, используемых в нескольких проектах Grandway

## Установка и использование

1. Добавить UIKit в зависимости в `package.json` проекта:
```json
{
  ...,
  "dependencies": {
    ...,
    "uikit": "git+https://npm-link:hCGkRjMccZaQauSQuCyA@gitlab.kozhindev.com/grandway/frontend-ui#<COMMIT_SHA>"
  }
}
```
где `<COMMIT_SHA>` - SHA-код нужного коммита в ветке `master` данного UIKit'а

2. Импортировать элементы из UIKit'а и использовать их в коде:
```js
import FAQSection from 'uikit/lendingsComponents/FAQSection';

function render() {
    return (
        <FAQSection
            someProp={'foo'}
        />
    )
}
```

## Обновление UIKit

При внесении изменений в UIKit необходимо:
1. Запушить в мастер необходимые изменения
2. Взять SHA-отпечаток коммита - их можно взять, например, на странице https://gitlab.kozhindev.com/grandway/frontend-ui/-/commits/master
3. В репозитории, использующем UIKit:
   1. в зависимости `uikit` в `package.json` заменить SHA-отпечаток на отпечаток из п.2
   2. обновить `yarn.lock`, запустив `yarn`