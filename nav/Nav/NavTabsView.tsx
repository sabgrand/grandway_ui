import * as React from 'react';

import {bem} from '@steroidsjs/core/hoc';
import Button from '@steroidsjs/core/ui/form/Button';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';
import {INavViewProps} from '@steroidsjs/core/ui/nav/Nav/Nav';

@bem('NavTabsView')
export default class NavTabsView extends React.Component<INavViewProps & IBemHocOutput> {

    render() {
        const bem = this.props.bem;
        return (
            <div className={bem(bem.block(), this.props.className)}>
                {this.props.items.map((item, index) => (
                    <Button
                        key={item.id || index}
                        className={bem(
                            bem.element('tab', {
                                'active': item.isActive,
                                'has-badge': item.badge >= 0,
                            }),
                            item.className
                        )}
                        layout={false}
                        link
                        onClick={e => {
                            e.preventDefault();
                            this.props.onClick(item, index);
                        }}
                        {...item}
                    />
                ))}
                {this.props.children}
            </div>
        );
    }
}
