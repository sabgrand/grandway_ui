import {SCREE_PHONE, SCREEN_DESKTOP, SCREEN_TABLET} from '@steroidsjs/core/reducers/screen';

// Referral
export const REFERRAL_CODE_LENGTH = 9;

// Screen decorator config
export const SCREEN_CONFIG = {
    [SCREE_PHONE]: 620,
    [SCREEN_TABLET]: 768,
    [SCREEN_DESKTOP]: 1024
};

const urlAccount = 'https://account.grandway.world';
export const urlAccountRegistration = urlAccount + '/auth/registration';
export const urlAccountLogin = urlAccount + '/auth/login';
