import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

interface IStorySectionProps extends IBemHocOutput {
    headerTitle?: string,
}

import './StorySection.scss'

@bem('StorySection')
export default class StorySection extends React.PureComponent<IStorySectionProps> {
    render() {
        const bem = this.props.bem;
        return(
            <section className={bem.block()}>
                {this.props.headerTitle && (
                    <h2 className={bem.element('header')}>
                        {this.props.headerTitle}
                    </h2>
                )}
                <div className='row'>
                    {this.props.children}
                </div>
            </section>
        );
    }
}