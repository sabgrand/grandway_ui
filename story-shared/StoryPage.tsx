import * as React from 'react';
import {bem} from '@steroidsjs/core/hoc';
import {IBemHocOutput} from '@steroidsjs/core/hoc/bem';

interface IStoryPageProps extends IBemHocOutput{
    headerTitle?: string,
    headerDescription?: string[],
}

import './StoryPage.scss'

@bem('StoryPage')
export default class StoryPage extends React.PureComponent<IStoryPageProps> {
    render() {
        const bem = this.props.bem;
        return(
            <div className={bem.block()}>
                {this.props.headerTitle && (
                    <div className={bem.element('header')}>
                        <h1 className={bem.element('title')}>
                            {this.props.headerTitle}
                        </h1>
                        {this.props.headerDescription && this.props.headerDescription.map((item, index) => (
                            <p key={index}>
                                {item}
                            </p>
                        ))}
                    </div>
                )}
                {this.props.children}
            </div>
        );
    }
}