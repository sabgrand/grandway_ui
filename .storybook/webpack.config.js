const path = require('path');

module.exports = require('@steroidsjs/webpack/config.storybook')({
    sourcePath: path.resolve(__dirname, '..'),
    webpack: {
        resolve: {
            modules: [
                path.resolve(__dirname, '../node_modules'),
                path.resolve(__dirname, '..'),
            ],
        },
    },
});
