import React from 'react';
import {addDecorator} from '@storybook/react';

import Application from '../base/Application';

// Wrap to component for call 'getStory' after application initialization
const Story = props => props.getStory();

// Add Application wrapper for init components, redux ant etc..
addDecorator(getStory => (
    <Application>
        <Story getStory={getStory}/>
    </Application>
));
