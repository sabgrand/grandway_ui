import {HttpComponent} from '@steroidsjs/core/components';
import {IConnectHocOutput} from '@steroidsjs/core/hoc/connect';
import {showNotification} from '@steroidsjs/core/actions/notifications';

interface IHttpRequestOptions extends IConnectHocOutput {
    lazy?: boolean | number,
    cancelToken?: any,
    onTwoFactor?: (providerName: string) => Promise<any>
}

export default class OwnHttpComponent extends HttpComponent {
    constructor(components, config) {
        super(components, config);

        this._components = components;
    }

    _sendAxios(config, options: IHttpRequestOptions) {
        const promise = super._sendAxios(config, options);
        return promise
            .catch(error => {
                if (error.response.status >= 500) {
                    const callServerErrorNotification = showNotification(
                        __('Ошибка при запросе к серверу. Попробуйте перезагрузить страницу'),
                        'danger',
                    );
                    this._components.store.dispatch(callServerErrorNotification);
                }
                throw error;
            });
    }
}
